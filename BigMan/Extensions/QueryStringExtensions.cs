﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Routing;

namespace BigMan.Extensions
{
    public static class QueryStringExtensions
    {
        public static RouteValueDictionary ToRouteValues(this NameValueCollection col, Object obj)
        {
            var values = obj == null ? new RouteValueDictionary() : new RouteValueDictionary(obj);
            if (col == null)
            {
                return values;
            }

            foreach (var key in col.Cast<string>().Where(key => !values.ContainsKey(key)))
            {
                values[key] = col[key];
            }

            return values;
        }
    }

}
