﻿using System;
using System.IO;
using BigMan.Annotations;
using BigMan.Domain;
using System.Collections.Generic;
using System.Linq;

namespace BigMan.Repository
{
    [UsedImplicitly]
    public class MovieRepository : IRepository<Movie>
    {
        private static IList<Movie> _moviesList;
        private static readonly string DataFilePath = String.Format("{0}MovieData.txt", Configuration.BasePath);

        IEnumerable<Movie> IRepository<Movie>.Retrieve()
        {
            return _moviesList ?? (_moviesList = File.ReadLines(DataFilePath).Select(Movie.FromExportFormat).OrderByDescending(x => x.Score()).ThenBy(x => x.Name).ToList());
        }
    }
}