﻿var SignalRHelper = {
    WireUpEvents: function () {
        $.ajax({
            url: "/signalr/hubs",
            dataType: "script",
            async: false
        });

        window.hub = $.connection.bigmanActions;
        $.connection.hub.start();
    },
};
