﻿using System.Web.Http;
using BigMan.Domain;
using BigMan.Portal.Models;
using BigMan.Services.Interfaces;

namespace BigMan.Portal.Controllers
{
    public class SearchController : ApiController
    {
        private readonly IMovieService _service;

        public SearchController(IMovieService service)
        {
            _service = service;
        }

        public MovieViewModel Get(string name)
        {
            if (string.IsNullOrEmpty(name) || string.IsNullOrWhiteSpace(name))
            {
                var movie = Defaults.DefaultMovie().MapToViewModel();
                movie.Plot = Defaults.DefaultMovieInformation().Plot;
                movie.ImageUrl = Defaults.DefaultMovieInformation().ImageUrl;
            }

            return _service.Retrieve(name);
        }
    }
}
