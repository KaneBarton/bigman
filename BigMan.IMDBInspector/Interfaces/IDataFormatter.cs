﻿using BigMan.Domain;
using System.Collections.Generic;

namespace BigMan.IMDBInspector.Interfaces
{
    interface IDataFormatter
    {
        Movie CurrentEnumerator { get; }
        IEnumerable<Movie> Parse();
        Movie ApplyValues(Movie source);
    }
}