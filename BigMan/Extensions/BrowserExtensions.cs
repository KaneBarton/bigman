using System.Collections.Generic;
using System.Web;

namespace BigMan.Extensions
{
    public static class BrowserExtensions
    {
        private static readonly HashSet<string> Browsers = new HashSet<string>(new[] { "6", "6.0", "7.0", "8.0", "8" });

        public static bool IsLegacyIEBrowser(this HttpBrowserCapabilitiesBase browser)
        {
            return browser.IsIE() && Browsers.Contains(browser.Version);
        }

        public static bool IsIE(this HttpBrowserCapabilitiesBase browser)
        {
            return browser.Browser.Equals("IE");
        }
    }
}