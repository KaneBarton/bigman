﻿var RecentlySearchedHelper = {
    WireUpEvents: function () {
        $.ajax({
            type: 'GET',
            cache: false,
            url: '/api/RecentlySearchedHub/',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                window.movieViewModel.RecentSearches(data.reverse());
                RecentlySearchedHelper.HideRecentSearchesLoadingSpinner();
            }
        });
    },

    HideRecentSearchesLoadingSpinner: function () {
        window.movieViewModel.ShowRecentSearches(true);
        window.movieViewModel.RecentSearchesLoading(false);
    },
};

function ClickToSearch(sender) {
    window.movieViewModel.SearchForMovieName(sender.name);
    window.movieViewModel.SearchForMovie();

    WaypointHelper.ScrollTo(2);
}