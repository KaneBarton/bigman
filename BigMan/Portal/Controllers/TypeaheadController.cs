﻿using System.Collections.Generic;
using System.Web.Http;
using BigMan.Services.Interfaces;

namespace BigMan.Portal.Controllers
{
    public class TypeaheadController : ApiController
    {
        private readonly IMovieNameSearchService _service;

        public TypeaheadController(IMovieNameSearchService service)
        {
            _service = service;
        }

        public IEnumerable<string> Get(string q)
        {
            return _service.Retrieve(q);
        }
    }
}