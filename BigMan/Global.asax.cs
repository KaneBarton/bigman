﻿using System.Net;
using BigMan.App_Start;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using BigMan.Extensions;
using BigMan.Services;

namespace BigMan
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            // Filters
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);

            // Routes            
            WebApiConfig.Register(GlobalConfiguration.Configuration);

            // SignalR
            RouteTable.Routes.MapHubs();

            RouteConfig.RegisterRoutes(RouteTable.Routes);

            // Bundles
            BundleScripts.RegisterBundles(BundleTable.Bundles);
            BundleStyles.RegisterBundles(BundleTable.Bundles);

            // OAuth, OpenId
            AuthConfig.RegisterAuth();

            // Application
            Repository.Configuration.BasePath = Server.MapPath("~/App_Data/");

            // Proxy
            WebRequest.DefaultWebProxy = new WebProxy();
            LocalStorage.ContentDownloadPath = Server.MapPath("/");
        }
    }
}