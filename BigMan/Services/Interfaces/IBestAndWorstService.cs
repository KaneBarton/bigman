﻿using BigMan.Portal.Models;

namespace BigMan.Services.Interfaces
{
    public interface IBestAndWorstService
    {
        BestAndWorstViewModel Retrieve(int year);
    }
}