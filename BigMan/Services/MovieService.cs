﻿using BigMan.Annotations;
using BigMan.Domain;
using BigMan.Portal.Models;
using BigMan.Repository;
using System.Linq;
using BigMan.Services.Interfaces;

namespace BigMan.Services
{
    [UsedImplicitly]
    public class MovieService : IMovieService
    {
        private readonly IMovieInformationService _movieInformationService;
        private readonly IManTagService _manTagService;
        private readonly IServiceCache<MovieViewModel> _serviceCache;
        private readonly IRepository<Movie> _movieRepository;

        public MovieService(IMovieInformationService movieInformationService, IManTagService manTagService, IServiceCache<MovieViewModel> serviceCache, IRepository<Movie> movieRepository)
        {
            _movieInformationService = movieInformationService;
            _manTagService = manTagService;
            _serviceCache = serviceCache;
            _movieRepository = movieRepository;
        }

        public MovieViewModel Retrieve(string name)
        {
            var nameUpper = name.ToUpperInvariant();

            if (_serviceCache.ContainsByKey(nameUpper)) return _serviceCache.ItemByKey(nameUpper);

            var domainModel = _movieRepository.Retrieve().FirstOrDefault(x => x.NameUpper.Contains(nameUpper)) ?? Defaults.DefaultMovie();

            var viewModel = domainModel.MapToViewModel();
            if (domainModel.IsDefault())
            {
                viewModel.ImageUrl = Defaults.DefaultMovieInformation().ImageUrl;
                viewModel.Plot = Defaults.DefaultMovieInformation().Plot;
            }
            else
            {
                var movieInformation = _movieInformationService.Retrieve(domainModel);
                viewModel.Plot = movieInformation.Plot;
                viewModel.ImageUrl = movieInformation.ImageUrl;
            }

            viewModel.ManTag = _manTagService.Retrieve(domainModel.Score());

            _serviceCache.Add(viewModel);

            return _serviceCache.Item(viewModel);
        }
    }
}