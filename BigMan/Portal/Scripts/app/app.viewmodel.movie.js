﻿var MovieViewModelHelper = {
    WireUpEvents: function () {
        window.movieViewModel = {
            SearchText: ko.observable('Search'),
            SearchForMovieName: ko.observable(''),
            Name: ko.observable('You gotta try searching for it...'),
            Plot: ko.observable(''),
            Year: ko.observable(0),
            ManTag: ko.observable(''),
            Score: ko.observable(0),
            ImageUrl: ko.observable('/Portal/Content/Images/no-poster.jpg'),
            ShowRecentSearches: ko.observable(false),
            RecentSearchesLoading: ko.observable(true),
            RecentSearches: ko.observableArray(),

            SearchForMovie: function () {
                var self = this;

                //if ($.trim(self.SearchForMovieName()) == '') return;

                SearchHelper.StartSearching();

                // the typeahead causes problems for knockout (this fixes it)
                self.SearchForMovieName($('#MovieNameSearch').val());

                $.getJSON('/api/Search/?name=' + self.SearchForMovieName(), function (data) {
                    SearchHelper.MapModelToViewModel(self, data);
                    SearchHelper.CompleteSearching();
                });
            }
        };
        
        ko.applyBindings(window.movieViewModel, App.Section2[0]);
    }
};