﻿using System.Linq;
using BigMan.Annotations;
using BigMan.Domain;
using BigMan.Extensions;
using BigMan.Repository;
using BigMan.Services.Interfaces;

namespace BigMan.Services
{
    [UsedImplicitly]
    public class ManTagService : IManTagService
    {
        private readonly IRepository<ManTag> _manTagRepository;

        public ManTagService(IRepository<ManTag> manTagRepository)
        {
            _manTagRepository = manTagRepository;
        }

        public string Retrieve(int score)
        {
            return _manTagRepository.Retrieve().Where(x => x.Score == score).PickRandom().Tag;
        }
    }
}