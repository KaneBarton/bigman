﻿using BigMan.Annotations;
using BigMan.Domain;
using BigMan.Extensions;
using BigMan.Services.Interfaces;

namespace BigMan.Services.MovieInformationLookups
{
    [UsedImplicitly]
    public class BingMovieLookup : BaseMovieLookup
    {
        private readonly IBingSearch _searchEngine;
        private readonly IIMDBLookup _imdbLookup;

        public BingMovieLookup(IBingSearch searchEngine, IIMDBLookup imdbLookup)
        {
            _searchEngine = searchEngine;
            _imdbLookup = imdbLookup;
        }

        public override int OrderSequence
        {
            get { return 3; }
        }

        public override bool TryParseResponse(string jsonResponse, out MovieInformation movieInformation)
        {
            movieInformation = new MovieInformation();

            // hit bing's API to get the IMDB movie number
            var bingSearch = _searchEngine.SearchWeb(MovieName);

            if (bingSearch != null)
            {
                string imdbId;
                if (WebRequestExtensions.TryParseImdbId(bingSearch.DisplayUrl, out imdbId))
                {
                    movieInformation = _imdbLookup.Retrieve(imdbId);
                    return true;
                }
            }

            return false;
        }

        protected override string BuildAPIUrl(string movieName, int year)
        {
            return string.Empty;
        }
    }
}