﻿using System.Web;
using BigMan.Domain;
using BigMan.Extensions;
using BigMan.Services.Interfaces;

namespace BigMan.Services.MovieInformationLookups
{
    public class DeanClatworthyLookup : BaseMovieLookup
    {
        private readonly IIMDBLookup _imdbLookup;

        public DeanClatworthyLookup(IIMDBLookup imdbLookup)
        {
            _imdbLookup = imdbLookup;
        }

        public override int OrderSequence
        {
            get { return -1; }
        }

        public override bool TryParseResponse(string jsonResponse, out MovieInformation movieInformation)
        {
            movieInformation = new MovieInformation();
            if (string.IsNullOrWhiteSpace(jsonResponse)) return false;

            if (!jsonResponse.StartsWith("imdbid")) return false;

            var movieData = jsonResponse.Split('|');

            if (movieData.Length > 2 && movieData[0] == "imdbid")
            {
                string imdbId;
                if (WebRequestExtensions.TryParseImdbId(movieData[2], out imdbId))
                {
                    movieInformation = _imdbLookup.Retrieve(imdbId);
                    return true;
                }
            }

            return false;
        }

        protected override string BuildAPIUrl(string movieName, int year)
        {
            return string.Format("http://deanclatworthy.com/imdb/?type=text&year={0}&q={1}", year, HttpUtility.UrlEncode(movieName));
        }
    }
}