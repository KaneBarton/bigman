﻿using System.Globalization;

namespace BigMan.Extensions
{
    public static class IntegerExtensions
    {
        public static string ToTwoDigitYear(this int value)
        {
            var prefix = string.Empty;
            if (value < 10)
            {
                prefix = "0";
            }

            return prefix + value.ToString(CultureInfo.InvariantCulture);
        }
    }
}