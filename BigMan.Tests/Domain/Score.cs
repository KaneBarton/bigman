﻿using BigMan.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BigMan.Tests.Domain
{
    public partial class MovieTests
    {
        [TestClass]
        public class Score
        {
            [TestMethod]
            public void Returns1WhenManScoreIsZero()
            {
                // Arrange
                var mock = new Mock<Movie>();
                mock.Setup(x => x.ManScore).Returns(0);

                // Act
                var score = mock.Object.Score();

                // Assert
                Assert.IsTrue(score == 1);
            }

            [TestMethod]
            public void ReturnsMoreThan1WhenManScoreGreaterThanOne()
            {
                // Arrange
                var mock = new Mock<Movie>();
                mock.Setup(x => x.ManScore).Returns(2);
                mock.Setup(x => x.MaxManScore).Returns(100);

                // Act
                var score = mock.Object.Score();

                // Assert
                Assert.IsTrue(score > 1);
            }
        }
    }
}