﻿using BigMan.Portal.Models;

namespace BigMan.Services.Interfaces
{
    public interface IMovieService
    {
        MovieViewModel Retrieve(string name);
    }
}