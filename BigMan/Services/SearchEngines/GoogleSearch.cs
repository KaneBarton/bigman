﻿using System;
using System.Linq;
using BigMan.Annotations;
using BigMan.Services.Interfaces;
using Google.Apis.Customsearch.v1;
using Google.Apis.Customsearch.v1.Data;
using Google.Apis.Services;

namespace BigMan.Services.SearchEngines
{
    [UsedImplicitly]
    public class GoogleSearch : IGoogleSearch
    {
        private static readonly BaseClientService.Initializer Initializer = new BaseClientService.Initializer
        {
            ApiKey = "AIzaSyBj0xLOVQFP_ykMCsvoc3mslPCIZ-sMaNI",
            ApplicationName = "Big Man",
        };

        public Result SearchWeb(string movieName)
        {
            try
            {
                var service = new CustomsearchService(Initializer);
                var request = service.Cse.List(movieName);
                request.Cx = "002681121051117809948:jsfnmqvglua";

                return request.Execute().Items.FirstOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}