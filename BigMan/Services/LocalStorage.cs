﻿using System;
using System.IO;
using System.Net;
using BigMan.Domain;

namespace BigMan.Services
{
    public static class LocalStorage
    {
        public static string ContentDownloadPath = String.Empty;
        private static readonly object CacheImageLock = new object();
        private static readonly object CacheImdbLock = new object();

        public static string GetMovieInformation(MovieInformation movieInformation)
        {
            var localFolder = Path.Combine(ContentDownloadPath, "Portal", "Content", "Downloaded", "MoveInfo", movieInformation.FolderName);
            var localFile = Path.Combine(localFolder, String.Format("{0}.txt", movieInformation.MovieSafeName));
            return File.Exists(localFile) ? File.ReadAllText(localFile) : String.Empty;
        }

        public static void StoreMovieInformation(MovieInformation movieInformation)
        {
            var localFolder = Path.Combine(ContentDownloadPath, "Portal", "Content", "Downloaded", "MoveInfo", movieInformation.FolderName);
            var localFile = Path.Combine(localFolder, String.Format("{0}.txt", movieInformation.MovieSafeName));

            if (!File.Exists(localFile))
            {
                lock (CacheImdbLock)
                {
                    if (!Directory.Exists(localFolder))
                    {
                        Directory.CreateDirectory(localFolder);
                    }

                    if (!File.Exists(localFile))
                    {
                        File.WriteAllText(localFile, movieInformation.ToString());
                    }
                }
            }
        }

        public static string MovieImage(string uri)
        {
            var localName = new Uri(uri).AbsolutePath.Replace("/", "-");
            var imageSubPath = String.Empty;
            if (localName.StartsWith("-images-"))
            {
                localName = localName.Substring(10);
                if (localName.Length > 8)
                {
                    imageSubPath = localName.Substring(0, 7) + "/";
                }
            }

            var localFolder = Path.Combine(ContentDownloadPath, "Portal", "Content", "Downloaded", imageSubPath.Replace("/", @"\"));
            var localFullFilePath = Path.Combine(localFolder, localName);
            var localUri = String.Format("/Portal/Content/Downloaded/{0}{1}", imageSubPath, localName);

            if (File.Exists(localFullFilePath)) return localUri;

            using (var client = new WebClient())
            {
                client.Proxy = null;
                var image = client.DownloadData(uri);

                lock (CacheImageLock)
                {
                    if (!Directory.Exists(localFolder))
                    {
                        Directory.CreateDirectory(localFolder);
                    }

                    if (!File.Exists(localFullFilePath))
                    {
                        File.WriteAllBytes(localFullFilePath, image);
                    }
                }
            }

            return localUri;
        }
    }
}