﻿using System.Web;
using BigMan.Annotations;
using BigMan.Domain;
using Newtonsoft.Json;

namespace BigMan.Services.MovieInformationLookups
{
    public class OmdbLookup : BaseMovieLookup
    {
        public override int OrderSequence
        {
            get { return 2; }
        }

        public override bool TryParseResponse(string jsonResponse, out MovieInformation movieInformation)
        {
            movieInformation = new MovieInformation();

            if (string.IsNullOrWhiteSpace(jsonResponse)) return false;

            var movieData = JsonConvert.DeserializeObject<JMovie>(jsonResponse);
            if (movieData.IsValid())
            {
                movieInformation.Plot = movieData.Plot;
                movieInformation.ImageUrl = movieData.Poster;
            }

            return movieData.IsValid();
        }

        protected override string BuildAPIUrl(string movieName, int year)
        {
            if (movieName.Length < 8)
            {
                // there is no (year) in the name so execute without parsing it
                return string.Format("http://www.omdbapi.com/?plot=full&i=&y={0}&t={1}", year, HttpUtility.UrlEncode(movieName));
            }

            // strip the (year) from the end of the movie name
            movieName = movieName.Substring(0, movieName.Length - 6).Trim();
            return string.Format("http://www.omdbapi.com/?plot=full&i=&y={0}&t={1}", year, HttpUtility.UrlEncode(movieName));
        }

        [UsedImplicitly]
        private class JMovie
        {
            // ReSharper disable MemberCanBePrivate.Local
            public string Response { get; set; }
            // ReSharper restore MemberCanBePrivate.Local

            public string Poster { get; set; }
            public string Plot { get; set; }

            public bool IsValid()
            {
                return Response == bool.TrueString && !string.IsNullOrEmpty(Poster) && !string.IsNullOrEmpty(Plot) && Poster.StartsWith("http");
            }
        }
    }
}