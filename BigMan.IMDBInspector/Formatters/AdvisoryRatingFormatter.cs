﻿using BigMan.Domain;
using BigMan.IMDBInspector.Interfaces;
using BigMan.IMDBInspector.Models;
using System;
using System.Collections.Generic;
using System.IO;

namespace BigMan.IMDBInspector.Formatters
{
    public class AdvisoryRatingFormatter : IDataFormatter
    {
        public Movie CurrentEnumerator { get; private set; }

        public IEnumerable<Movie> Parse()
        {
            var lineNumber = 0;
            var model = new AdvisoryRatingModel();

            foreach (var line in File.ReadLines(Path.Combine(Program.BasePath, "mpaa-ratings-reasons.list")))
            {
                lineNumber++;
                if (lineNumber % 50000 == 1) Console.Write(".");
                if (lineNumber < 344) continue;

                if (line.StartsWith("MV:"))
                {
                    model = new AdvisoryRatingModel { Name = line.Substring(4).Trim() };
                    continue;
                }

                if (!line.StartsWith("RE: Rated "))
                {
                    continue;
                }

                model.Rating = line.Substring(10);
                model.Rating = model.Rating.Substring(0, model.Rating.IndexOf(" ", System.StringComparison.Ordinal));

                CurrentEnumerator =  new Movie(model.Name) { AdvisoryRatingMultipler = model.Score() };
                yield return CurrentEnumerator;
            }
        }

        public Movie ApplyValues(Movie source)
        {
            source.AdvisoryRatingMultipler = CurrentEnumerator.AdvisoryRatingMultipler;
            return source;
        }
    }
}