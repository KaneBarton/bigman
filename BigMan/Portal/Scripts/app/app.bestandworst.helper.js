﻿var BestAndWorstHelper = {
    WireUpEvents: function () {
        BestAndWorstHelper.SearchForYear(0);
    },
    
    SearchForYear: function (year) {
        window.bestAndWorstViewModel.SearchesLoading(true);
        window.bestAndWorstViewModel.SearchesLoaded(false);

        var apiUrl = '/api/BestAndWorst/';

        if (year != 0) {
            apiUrl += '?year=' + year;
        }

        $.ajax({
            type: 'GET',
            cache: false,
            url: apiUrl,
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                window.bestAndWorstViewModel.Year(data.year);
                window.bestAndWorstViewModel.BestResults(data.best);

                if (window.App.WorstResultsShown) {
                    window.bestAndWorstViewModel.WorstResults(data.worst);
                }

                window.bestAndWorstViewModel.SearchesLoading(false);
                window.bestAndWorstViewModel.SearchesLoaded(true);
            }
        });
    }
}