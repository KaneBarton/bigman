﻿using BigMan.Domain;
using BigMan.IMDBInspector.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;

namespace BigMan.IMDBInspector.Formatters
{
    public class YearFormatter : IDataFormatter
    {
        public Movie CurrentEnumerator { get; private set; }

        public IEnumerable<Movie> Parse()
        {
            var lineNumber = 0;

            foreach (var line in File.ReadLines(Path.Combine(Program.BasePath, "movies.list")))
            {
                lineNumber++;
                if (lineNumber % 50000 == 1) Console.Write(".");
                if (lineNumber < 16) continue;
                if (line.StartsWith("---------")) continue;

                var movieName = line.Substring(0, line.IndexOf("\t", System.StringComparison.Ordinal)).Trim();
                var year = line.Substring(movieName.Length + 1).Trim();
                int yearInt;

                if (!int.TryParse(year, out yearInt)) continue;
               
                CurrentEnumerator = new Movie(movieName) { Year = yearInt };
                yield return CurrentEnumerator;
            }
        }

        public Movie ApplyValues(Movie source)
        {
            source.Year = CurrentEnumerator.Year;
            return source;
        }
    }
}