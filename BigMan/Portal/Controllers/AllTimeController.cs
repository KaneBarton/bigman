﻿using System.Collections.Generic;
using System.Web.Http;
using BigMan.Portal.Models;
using BigMan.Services.Interfaces;

namespace BigMan.Portal.Controllers
{
    public class AllTimeController : ApiController
    {
         private readonly IAllTimeService _service;

         public AllTimeController(IAllTimeService service)
        {
            _service = service;
        }

        public IEnumerable<MovieViewModel> Get()
        {
            return _service.Retrieve();
        }
    }
}
