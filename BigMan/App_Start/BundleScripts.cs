﻿using System.Web.Optimization;

namespace BigMan.App_Start
{
    public static class BundleScripts
    {
        public static string Modernizr
        {
            get { return "~/Portal/ModernirBundle"; }
        }

        public static string Core
        {
            get { return "~/Portal/CoreBundle"; }
        }

        public static string App
        {
            get { return "~/Portal/AppBundle"; }
        }

        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle(Core).Include(
                "~/Portal/Scripts/jquery-{version}.js",
                "~/Portal/Scripts/jquery.easing*",
                "~/Portal/Scripts/jquery.backstretch.js",
                "~/Portal/Scripts/jquery.signalR-1.1.3.js",
                "~/Portal/Scripts/knockout-{version}.js",
                "~/Portal/Scripts/knockout.mapping-latest.js",
                "~/Portal/Scripts/perpetuum.knockout.js",
                "~/Portal/Scripts/typeahead*",
                "~/Portal/Scripts/bootstrap*",
                "~/Portal/Scripts/underscore.js",
                "~/Portal/Scripts/underscore-ko-{version}.js",
                "~/Portal/Scripts/waypoints.min.js"
            ));

            bundles.Add(new ScriptBundle(App).Include(
                "~/Portal/Scripts/app/app.js",
                "~/Portal/Scripts/app/app.*",
                "~/Portal/Scripts/app.chart/app.*"
            ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle(Modernizr).Include("~/Portal/Scripts/modernizr-*"));
        }
    }
}