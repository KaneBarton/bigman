﻿using System.Collections.Generic;
using BigMan.Portal.Models;

namespace BigMan.Services.Interfaces
{
    public interface IAllTimeService
    {
        IEnumerable<MovieViewModel> Retrieve();
    }
}