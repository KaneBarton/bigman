﻿using BigMan.Domain;

namespace BigMan.Services.Interfaces
{
    public interface IMovieInformationService
    {
        MovieInformation Retrieve(Movie movie);
        MovieInformation Retrieve(string movieName, int year);
    }
}