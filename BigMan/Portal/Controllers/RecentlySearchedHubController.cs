﻿using System.Collections.Generic;
using System.Linq;
using BigMan.Domain;
using BigMan.Portal.Controllers.Base;
using BigMan.Portal.Infrastructure.ReatimeCollaboration;
using BigMan.Portal.Models;
using BigMan.Portal.Models.HubModels;
using BigMan.Services.Interfaces;

namespace BigMan.Portal.Controllers
{
    public class RecentlySearchedHubController : ApiHubBase<BigManActionsHub>
    {
        private readonly IMovieInformationService _service;
        private static readonly IList<RecentlySearchedViewModel> RecentlyViewed = new List<RecentlySearchedViewModel>();
        private static readonly IList<string> RecentlyViewedMovieNames = new List<string>();
        private static readonly object PadLock = new object();

        public RecentlySearchedHubController(IMovieInformationService service)
        {
            _service = service;
        }

        // SignalR
        public void Post(RecentlySearchedHubModel message)
        {
            var movieName = message.MovieName ?? Defaults.DefaultMovie().Name;

            AddToRecentlyViewed(movieName, message.Year);

            Hub.Clients.All.searchedFor(RecentlyViewed.ToArray());
        }

        // Normal WebAPI
        public IEnumerable<RecentlySearchedViewModel> Get()
        {
            if (RecentlyViewed.Count == 0)
            {
                lock (PadLock)
                {
                    if (RecentlyViewed.Count == 0)
                    {
                        AddToRecentlyViewed("Watchmen (2009)", 2009);
                        AddToRecentlyViewed("Iron Man (2008)", 2008);
                        AddToRecentlyViewed("Die Hard (1988)", 1988);
                        AddToRecentlyViewed("The Usual Suspects (1995)", 1995);
                        AddToRecentlyViewed("300 (2006)", 2006);
                        AddToRecentlyViewed("Fight Club (1999)", 1999);
                    }
                }
            }

            return RecentlyViewed.ToArray();
        }

        private void AddToRecentlyViewed(string movieName, int year)
        {
            var movieNameUpper = movieName.ToUpperInvariant();

            if (RecentlyViewedMovieNames.Contains(movieNameUpper))
            {
                return;
            }

            RecentlyViewed.Add(new RecentlySearchedViewModel { Name = movieName, ImageUrl = _service.Retrieve(movieName, year).ImageUrl });
            RecentlyViewedMovieNames.Add(movieNameUpper);

            while (RecentlyViewed.Count > 6)
            {
                RecentlyViewed.RemoveAt(0);
                RecentlyViewedMovieNames.RemoveAt(0);
            }
        }
    }
}