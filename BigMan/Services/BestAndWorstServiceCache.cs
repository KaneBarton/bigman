﻿using System.Collections.Generic;
using System.Globalization;
using BigMan.Annotations;
using BigMan.Portal.Models;
using BigMan.Services.Interfaces;

namespace BigMan.Services
{
    [UsedImplicitly]
    public class BestAndWorstServiceCache : IServiceCache<BestAndWorstViewModel>
    {
        private static readonly IDictionary<string, BestAndWorstViewModel> Cache = new Dictionary<string, BestAndWorstViewModel>();
        private static readonly object BestAndWorstLock = new object(); // singleton locks as we are using static variables, as there really isn't a need to use the HttpRuntime.Cache

        public void Add(BestAndWorstViewModel domain)
        {
            if (Contains(domain)) return;

            lock (BestAndWorstLock)
            {
                if (!Contains(domain))
                {
                    Cache.Add(domain.Year.ToString(CultureInfo.InvariantCulture), domain);
                }
            }
        }

        private bool Contains(BestAndWorstViewModel domain)
        {
            return Cache.ContainsKey(domain.Year.ToString(CultureInfo.InvariantCulture));
        }

        public bool ContainsByKey(string key)
        {
            return Cache.ContainsKey(key);
        }

        public BestAndWorstViewModel Item(BestAndWorstViewModel domain)
        {
            return Cache[domain.Year.ToString(CultureInfo.InvariantCulture)];
        }

        public BestAndWorstViewModel ItemByKey(string key)
        {
            return Cache[key];            
        }
    }
}