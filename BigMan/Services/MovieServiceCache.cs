﻿using System.Collections.Generic;
using BigMan.Annotations;
using BigMan.Portal.Models;
using BigMan.Services.Interfaces;

namespace BigMan.Services
{
    [UsedImplicitly]
    public class MovieServiceCache : IServiceCache<MovieViewModel>
    {
        private static readonly IDictionary<string, MovieViewModel> Cache = new Dictionary<string, MovieViewModel>();
        private static readonly object MovieInformationLock = new object(); // singleton locks as we are using static variables, as there really isn't a need to use the HttpRuntime.Cache

        public void Add(MovieViewModel domain)
        {
            if (Contains(domain)) return;

            lock (MovieInformationLock)
            {
                if (!Contains(domain))
                {
                    Cache.Add(domain.NameUpper, domain);
                }
            }
        }

        private bool Contains(MovieViewModel domain)
        {
            return Cache.ContainsKey(domain.NameUpper);
        }

        public bool ContainsByKey(string key)
        {
            return Cache.ContainsKey(key.ToUpperInvariant());
        }

        public MovieViewModel Item(MovieViewModel domain)
        {
            return Cache[domain.NameUpper];
        }

        public MovieViewModel ItemByKey(string key)
        {
            return Cache[key.ToUpperInvariant()];
        }
    }
}