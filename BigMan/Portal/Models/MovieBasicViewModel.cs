﻿namespace BigMan.Portal.Models
{
    public class MovieBasicViewModel
    {
        public int Year { get; set; }
        public int Score { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
    }
}