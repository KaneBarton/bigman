// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IoC.cs" company="Web Advanced">
// Copyright 2012 Web Advanced (www.webadvanced.com)
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------


using BigMan.Domain;
using BigMan.Portal.Models;
using BigMan.Repository;
using BigMan.Services;
using BigMan.Services.Interfaces;
using BigMan.Services.SearchEngines;
using StructureMap;

namespace BigMan.Portal.Infrastructure.DependencyResolution
{
    public static class IoC
    {
        public static IContainer Initialize()
        {
            ObjectFactory.Initialize(x => x.Scan(scan =>
            {
                scan.TheCallingAssembly();
                scan.WithDefaultConventions();
                scan.AddAllTypesOf<IMovieInformationLookup>();
                x.For<IMovieInformationService>().Use<MovieInformationService>();
                x.For<IRepository<ManTag>>().Use<ManTagRepository>();
                x.For<IRepository<Movie>>().Use<MovieRepository>();
                x.For<IBingSearch>().Use<BingSearch>();
                x.For<IGoogleSearch>().Use<GoogleSearch>();
                x.For<IServiceCache<MovieViewModel>>().Use<MovieServiceCache>();
                x.For<IServiceCache<BestAndWorstViewModel>>().Use<BestAndWorstServiceCache>();
                x.For<IServiceCache<MovieInformation>>().Use<MovieInformationServiceCache>();
            }));
            return ObjectFactory.Container;
        }
    }
}