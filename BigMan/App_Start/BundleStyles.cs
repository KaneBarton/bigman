﻿using System.Web.Optimization;

namespace BigMan.App_Start
{
    public static class BundleStyles
    {
        public static string Default
        {
            get { return "~/Portal/Content/bigManBundle"; }
        }

        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle(Default).Include(
                "~/Portal/Content/bootstrap.css",
                "~/Portal/Content/bootstrap.overrides.css",
                "~/Portal/Content/font-awesome.css",
                "~/Portal/Content/typeahead.js-bootstrap.css",
                "~/Portal/Content/app.css",
                "~/Portal/Content/app.navigation.css",
                "~/Portal/Content/app.buttons.css",
                "~/Portal/Content/app.containers.css",
                "~/Portal/Content/app.sections.css",
                "~/Portal/Content/app.callout.css",
                "~/Portal/Content/app.easy-pie-chart.css",
                "~/Portal/Content/app.touch.css"));
        }
    }
}