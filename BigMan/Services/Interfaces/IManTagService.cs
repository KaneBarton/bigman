﻿namespace BigMan.Services.Interfaces
{
    public interface IManTagService
    {
        string Retrieve(int score);
    }
}