﻿using BigMan.Extensions;
using Newtonsoft.Json;
using System;

namespace BigMan.Portal.Models
{
    public class RecentlySearchedViewModel
    {
        public RecentlySearchedViewModel()
        {
            _dateAddeTime = DateTime.Now;
        }

        private DateTime _dateAddeTime;

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("imageUrl")]
        public string ImageUrl { get; set; }

        [JsonProperty("ago")]
        public string Ago
        {
            get { return _dateAddeTime.ToRelativeTime(DateTime.Now); }
        }
    }
}