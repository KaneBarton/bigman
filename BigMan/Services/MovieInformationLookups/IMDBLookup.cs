﻿using BigMan.Annotations;
using BigMan.Domain;
using BigMan.Extensions;
using BigMan.Services.Interfaces;
using Newtonsoft.Json;

namespace BigMan.Services.MovieInformationLookups
{
    [UsedImplicitly]
    public class IMDBLookup : IIMDBLookup
    {
        public MovieInformation Retrieve(string imdbIdentifier)
        {
            var movieInformation = new MovieInformation();

            var jsonResponse = WebRequestExtensions.HttpGet(string.Format("http://www.omdbapi.com/?plot=full&i={0}", imdbIdentifier));
            var movieData = JsonConvert.DeserializeObject<JMovie>(jsonResponse);

            if (!movieData.IsValid()) return Defaults.DefaultMovieInformation();

            movieInformation.ImageUrl = movieData.Poster;
            movieInformation.Plot = movieData.Plot;

            if (movieInformation.ImageUrl == "N/A")
            {
                movieInformation.ImageUrl = Defaults.Image;
            }

            return movieInformation;
        }

        [UsedImplicitly]
        private class JMovie
        {
            public string Poster { get; set; }
            public string Plot { get; set; }

            public bool IsValid()
            {
                return !(string.IsNullOrEmpty(Poster) && string.IsNullOrEmpty(Plot));
            }
        }
    }
}