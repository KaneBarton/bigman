﻿using System;
using System.Web.Http;
using BigMan.Portal.Models;
using BigMan.Services.Interfaces;

namespace BigMan.Portal.Controllers
{
    public class BestAndWorstController : ApiController
    {
        private readonly IBestAndWorstService _service;

        public BestAndWorstController(IBestAndWorstService service)
        {
            _service = service;
        }

        public BestAndWorstViewModel Get(int year)
        {
            return _service.Retrieve(year);
        }

        public BestAndWorstViewModel Get()
        {
            return _service.Retrieve(DateTime.Now.Year);
        }
    }
}