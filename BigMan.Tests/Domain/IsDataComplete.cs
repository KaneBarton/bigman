﻿using BigMan.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BigMan.Tests.Domain
{
    public partial class MovieTests
    {
        [TestClass]
        public class IsDataComplete
        {
            [TestMethod]
            public void ReturnsFalseWhenYearIsBelowMinimum()
            {
                // Arrange 
                var mock = new Mock<Movie>();
                mock.Setup(x => x.Year).Returns(1901);

                // Act
                var dataComplete = mock.Object.IsDataComplete();

                // Assert
                Assert.IsFalse(dataComplete);
            }

            [TestMethod]
            public void ReturnsFalseWhenAdvisoryRatingMultiplerIsBelowMinimum()
            {
                // Arrange 
                var mock = new Mock<Movie>();
                mock.Setup(x => x.AdvisoryRatingMultipler).Returns(0);

                // Act
                var dataComplete = mock.Object.IsDataComplete();

                // Assert
                Assert.IsFalse(dataComplete);
            }

            [TestMethod]
            public void ReturnsFalseWhenGenreMultiplierIsBelowMinimum()
            {
                // Arrange 
                var mock = new Mock<Movie>();
                mock.Setup(x => x.GenreMultiplier).Returns(0);

                // Act
                var dataComplete = mock.Object.IsDataComplete();

                // Assert
                Assert.IsFalse(dataComplete);
            }

            [TestMethod]
            public void ReturnsFalseWhenKeywordsMultiplerIsBelowMinimum()
            {
                // Arrange 
                var mock = new Mock<Movie>();
                mock.Setup(x => x.KeywordsMultipler).Returns(0);

                // Act
                var dataComplete = mock.Object.IsDataComplete();

                // Assert
                Assert.IsFalse(dataComplete);
            }

            [TestMethod]
            public void ReturnsFalseWhenPublicRatingMultiplerIsBelowMinimum()
            {
                // Arrange 
                var mock = new Mock<Movie>();
                mock.Setup(x => x.PublicRatingMultipler).Returns(0);

                // Act
                var dataComplete = mock.Object.IsDataComplete();

                // Assert
                Assert.IsFalse(dataComplete);
            }

            [TestMethod]
            public void ReturnsTrueWhenValuesAreCorrect()
            {
                // Arrange 
                var mock = new Mock<Movie>();
                mock.Setup(x => x.Year).Returns(Defaults.MinYear + 1);
                mock.Setup(x => x.AdvisoryRatingMultipler).Returns(1);
                mock.Setup(x => x.GenreMultiplier).Returns(1);
                mock.Setup(x => x.KeywordsMultipler).Returns(1);
                mock.Setup(x => x.PublicRatingMultipler).Returns(1);

                // Act
                var dataComplete = mock.Object.IsDataComplete();

                // Assert
                Assert.IsTrue(dataComplete);
            }
        }
    }
}