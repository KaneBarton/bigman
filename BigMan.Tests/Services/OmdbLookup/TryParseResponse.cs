﻿using BigMan.Annotations;
using BigMan.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BigMan.Tests.Services.OmdbLookup
{
    [UsedImplicitly]
    public partial class OmdbLookupTests
    {
        [TestClass]
        public class TryParseResponse
        {
            [TestMethod]
            public void WhenEmptyResponseThenNotValid()
            {
                // Arrange
                var lookup = new BigMan.Services.MovieInformationLookups.OmdbLookup();
                MovieInformation movieInformation;

                // Act
                var isValid = lookup.TryParseResponse(string.Empty, out movieInformation);

                // Assert
                Assert.IsFalse(isValid);
            }

            [TestMethod]
            public void WhenResponseHasFalseElementThenNotValid()
            {
                // Arrange
                var lookup = new BigMan.Services.MovieInformationLookups.OmdbLookup();
                MovieInformation movieInformation;

                // Act
                var isValid = lookup.TryParseResponse("{\"Response\":\"False\",\"Error\":\"Movie not found!\"}", out movieInformation);

                // Assert
                Assert.IsFalse(isValid);
            }

            [TestMethod]
            public void WhenResponseDoesNotContainPlotThenInValid()
            {
                // Arrange
                var lookup = new BigMan.Services.MovieInformationLookups.OmdbLookup();
                MovieInformation movieInformation;

                // Act
                var isValid = lookup.TryParseResponse("{\"Title\":\"Iron Man\",\"Year\":\"2008\",\"Rated\":\"PG-13\",\"Released\":\"02 May 2008\",\"Runtime\":\"2 h 6 min\",\"Genre\":\"Action, Adventure, Sci-Fi\",\"Director\":\"Jon Favreau\",\"Writer\":\"Mark Fergus, Hawk Ostby\",\"Actors\":\"Robert Downey Jr., Gwyneth Paltrow, Terrence Howard, Jeff Bridges\",\"Poster\":\"http://ia.media-imdb.com/images/M/MV5BMTczNTI2ODUwOF5BMl5BanBnXkFtZTcwMTU0NTIzMw@@._V1_SX300.jpg\",\"imdbRating\":\"7.9\",\"imdbVotes\":\"417,624\",\"imdbID\":\"tt0371746\",\"Type\":\"movie\",\"Response\":\"True\"}", out movieInformation);

                // Assert
                Assert.IsFalse(isValid);
            }

            [TestMethod]
            public void WhenResponseDoesNotContainPosterThenInValid()
            {
                // Arrange
                var lookup = new BigMan.Services.MovieInformationLookups.OmdbLookup();
                MovieInformation movieInformation;

                // Act
                var isValid = lookup.TryParseResponse("{\"Title\":\"Iron Man\",\"Year\":\"2008\",\"Rated\":\"PG-13\",\"Released\":\"02 May 2008\",\"Runtime\":\"2 h 6 min\",\"Genre\":\"Action, Adventure, Sci-Fi\",\"Director\":\"Jon Favreau\",\"Writer\":\"Mark Fergus, Hawk Ostby\",\"Actors\":\"Robert Downey Jr., Gwyneth Paltrow, Terrence Howard, Jeff Bridges\",\"Plot\":\"When wealthy industrialist Tony Stark is forced to build an armored suit after a life-threatening incident, he ultimately decides to use its technology to fight against evil.\",\"imdbRating\":\"7.9\",\"imdbVotes\":\"417,624\",\"imdbID\":\"tt0371746\",\"Type\":\"movie\",\"Response\":\"True\"}", out movieInformation);

                // Assert
                Assert.IsFalse(isValid);
            }

            [TestMethod]
            public void WhenResponseContainsPosterAndPlotThenValid()
            {
                // Arrange
                var lookup = new BigMan.Services.MovieInformationLookups.OmdbLookup();
                MovieInformation movieInformation;

                // Act
                var isValid = lookup.TryParseResponse("{\"Title\":\"Iron Man\",\"Year\":\"2008\",\"Rated\":\"PG-13\",\"Released\":\"02 May 2008\",\"Runtime\":\"2 h 6 min\",\"Genre\":\"Action, Adventure, Sci-Fi\",\"Director\":\"Jon Favreau\",\"Writer\":\"Mark Fergus, Hawk Ostby\",\"Actors\":\"Robert Downey Jr., Gwyneth Paltrow, Terrence Howard, Jeff Bridges\",\"Plot\":\"When wealthy industrialist Tony Stark is forced to build an armored suit after a life-threatening incident, he ultimately decides to use its technology to fight against evil.\",\"Poster\":\"http://ia.media-imdb.com/images/M/MV5BMTczNTI2ODUwOF5BMl5BanBnXkFtZTcwMTU0NTIzMw@@._V1_SX300.jpg\",\"imdbRating\":\"7.9\",\"imdbVotes\":\"417,624\",\"imdbID\":\"tt0371746\",\"Type\":\"movie\",\"Response\":\"True\"}", out movieInformation);

                // Assert
                Assert.IsTrue(isValid);
            }
        }
    }
}