﻿var AllTimeHelper = {
    WireUpEvents: function() {
        $.ajax({
            type: 'GET',
            cache: false,
            url: '/api/alltime/',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                window.allTimeViewModel.Results(data);
                window.allTimeViewModel.SearchesLoading(false);
                window.allTimeViewModel.SearchesLoaded(true);
            }
        });
    },
};