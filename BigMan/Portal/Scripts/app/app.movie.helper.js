﻿var SearchHelper = {
    WireUpEvents: function () {
        App.MovieNameSearch.pressEnter(function () {
            window.movieViewModel.SearchForMovie();
        });

        window.hub.client.searchedFor = function (data) {
            window.movieViewModel.RecentSearches(data.reverse());
            RecentlySearchedHelper.HideRecentSearchesLoadingSpinner();
        };
    },

    MapModelToViewModel: function (viewModel, domainModel) {
        viewModel.Name(domainModel.name);
        viewModel.Plot(domainModel.plot);
        viewModel.ManTag(domainModel.manTag);
        viewModel.Year(domainModel.year);
        viewModel.Score(domainModel.score);
        viewModel.ImageUrl(domainModel.imageUrl);

        RadialGaugeHelper.Update(domainModel.score);
    },

    StartSearching: function () {
        window.movieViewModel.SearchText('Searching...');
        window.movieViewModel.Name('...');
        window.movieViewModel.Plot('');
        window.movieViewModel.ManTag('');
        RadialGaugeHelper.Update(0);

        App.MoviePoster.hide();
        App.MoviePosterSearching.show();
        App.MovieNameSearchButtonIcon.removeClass('icon-search');
        App.MovieNameSearchButtonIcon.addClass('icon-refresh icon-spin');
    },

    CompleteSearching: function () {
        SearchHelper.HubSend();
        window.movieViewModel.SearchText('Search');
        window.movieViewModel.SearchForMovieName('');

        App.MoviePosterSearching.hide();
        App.MoviePoster.show();
        App.MovieNameSearchButtonIcon.removeClass('icon-refresh icon-spin');
        App.MovieNameSearchButtonIcon.addClass('icon-search');
    },

    HubSend: function () {
        var postData = JSON.stringify({
            connectionId: $.connection.hub.id,
            movieName: window.movieViewModel.SearchForMovieName(),
            year: window.movieViewModel.Year(),
        });

        $.ajax({
            type: 'POST',
            cache: false,
            url: '/api/RecentlySearchedHub/',
            contentType: 'application/json; charset=utf-8',
            data: postData,
        });
    }
};