﻿namespace BigMan.Services.Interfaces
{
    public interface IServiceCache<T>
    {
        void Add(T domain);
        bool ContainsByKey(string key);
        T Item(T domain);
        T ItemByKey(string key);
    }
}