﻿using BigMan.Domain;

namespace BigMan.Services.Interfaces
{
    public interface IMovieInformationLookup
    {
        int OrderSequence { get; }
        MovieInformation Retrieve(string movieName, int year);
    }
}