﻿var YearOnYearHelper = {
    WireUpEvents: function () {
        $.ajax({
            type: 'GET',
            cache: false,
            url: '/api/yearbyyear/',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                window.yearByYearViewModel.Results(data);
                window.yearByYearViewModel.SearchesLoading(false);
                window.yearByYearViewModel.SearchesLoaded(true);

                YearOnYearHelper.UpdateYearOnYear();
            }
        });
    },
    
    UpdateYearOnYear: function () {
        $('.progress-bar', '#YearOnYearResults').each(function () {
            var item = $(this);
            item.css('width', item.attr('aria-valuenow') + '%');
        });
    },

}