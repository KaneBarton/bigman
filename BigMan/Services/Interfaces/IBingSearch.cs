﻿using Bing;

namespace BigMan.Services.Interfaces
{
    public interface IBingSearch
    {
        WebResult SearchWeb(string searchFor);
    }
}