﻿using System;
using BigMan.Annotations;
using BigMan.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BigMan.Tests.Domain
{
    [UsedImplicitly]
    public partial class MovieTests
    {
        [TestClass]
        public class Export
        {
            [TestMethod]
            public void CanBeExportedAndImportedSuccessfully()
            {
                // Arrange
                var movie = new Movie(Guid.NewGuid().ToString("N"));

                // Act 
                var export = movie.ToExportFormat();
                var expectedMovie = Movie.FromExportFormat(export);

                // Assert
                Assert.AreEqual(movie.Name, expectedMovie.Name);
            }
        }
    }
}