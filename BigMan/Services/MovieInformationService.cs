﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using BigMan.Annotations;
using BigMan.Domain;
using BigMan.Extensions;
using BigMan.Services.Interfaces;

namespace BigMan.Services
{
    [UsedImplicitly]
    public class MovieInformationService : IMovieInformationService
    {
        private static IEnumerable<IMovieInformationLookup> _lookupServices;
        private readonly IServiceCache<MovieInformation> _serviceCache;

        public MovieInformationService(IEnumerable<IMovieInformationLookup> lookupServices, IServiceCache<MovieInformation> serviceCache)
        {
            if (_lookupServices == null)
            {
                _lookupServices = lookupServices.Where(x => x.OrderSequence != -1).OrderBy(x => x.OrderSequence).ToList();                
            }

            _serviceCache = serviceCache;
        }

        public MovieInformation Retrieve(Movie movie)
        {
            if (movie == null) return Defaults.DefaultMovieInformation();
            return Retrieve(movie.NameUpper, movie.Year);
        }

        public MovieInformation Retrieve(string movieName, int year)
        {
            if (string.IsNullOrEmpty(movieName)) return Defaults.DefaultMovieInformation();

            if (_serviceCache.ContainsByKey(movieName)) return _serviceCache.ItemByKey(movieName);

            foreach (var movieInformation in _lookupServices.Select(lookupService => lookupService.Retrieve(movieName, year)).Where(movieInformation => movieInformation != null))
            {
                if (movieInformation.ImageUrl != Defaults.DefaultMovieInformation().ImageUrl)
                {
                    movieInformation.ImageUrl = LocalStorage.MovieImage(movieInformation.ImageUrl);
                }

                _serviceCache.Add(movieInformation);
                return _serviceCache.Item(movieInformation);
            }

            return Defaults.DefaultMovieInformation();
        }
    }
}