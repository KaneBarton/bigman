﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Routing;

namespace BigMan.Portal.Infrastructure.HtmlHelpers
{
    // http://stackoverflow.com/questions/6255138/how-to-specify-id-for-an-html-labelfor-mvc-razor
    public static class LabelExtensionHelper
    {
        public static MvcHtmlString LabelFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string text, object htmlAttributes)
        {
            return LabelHelper(
                html,
                ModelMetadata.FromLambdaExpression(expression, html.ViewData),
                ExpressionHelper.GetExpressionText(expression),
                text,
                htmlAttributes
            );
        }

        public static MvcHtmlString Label(this HtmlHelper html, string htmlFieldName, string text, object htmlAttributes)
        {
            var tag = new TagBuilder("label");
            tag.Attributes.Add("for", htmlFieldName);
            tag.MergeAttributes(new RouteValueDictionary(htmlAttributes));
            tag.SetInnerText(text);
            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }

        private static MvcHtmlString LabelHelper(HtmlHelper html, ModelMetadata metadata, string htmlFieldName, string text, object htmlAttributes)
        {
            var tag = new TagBuilder("label");
            tag.Attributes.Add("for", TagBuilder.CreateSanitizedId(html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(htmlFieldName)));
            tag.MergeAttributes(new RouteValueDictionary(htmlAttributes));
            tag.SetInnerText(text);
            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }
    }
}