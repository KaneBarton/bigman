﻿using BigMan.Domain;
using BigMan.IMDBInspector.Models;
using System;
using System.Collections.Generic;
using System.IO;

namespace BigMan.IMDBInspector.Formatters
{
    public class GenreFormatter
    {

        public IEnumerable<Movie> Parse()
        {
            var movies = new Dictionary<string, GenreModel>();
            var movieNamesToExclude = new HashSet<string>();

            var lineNumber = 0;
            foreach (var line in File.ReadLines(Path.Combine(Program.BasePath, "genres.list")))
            {
                lineNumber++;
                if (GenreFormatter.SkipLines(lineNumber)) continue;

                var model = GenreFormatter.ParseStringToModel(line);
                if (model.Score() == 0 && !movieNamesToExclude.Contains(model.Name))
                {
                    movieNamesToExclude.Add(model.Name);
                }
            }

            Console.WriteLine();
            lineNumber = 0;
            foreach (var line in File.ReadLines(Path.Combine(Program.BasePath, "genres.list")))
            {
                lineNumber++;
                if (GenreFormatter.SkipLines(lineNumber)) continue;

                var model = GenreFormatter.ParseStringToModel(line);
                if (!model.NameIsValid) continue;
                if (movieNamesToExclude.Contains(model.Name)) continue;
                if (model.Name.Contains("�")) continue;
                
                if (movies.ContainsKey(model.Name))
                {
                    if (model.Score() > movies[model.Name].Score())
                    {
                        movies[model.Name] = model;
                    }
                }
                else
                {
                    movies.Add(model.Name, model);
                }

            }

            foreach (var movieGenre in movies.Values)
            {
                yield return new Movie(movieGenre.Name)
                    {
                        GenreMultiplier = movieGenre.Score(),
                        AdvisoryRatingMultipler = AdvisoryRatingModel.Default,
                        PublicRatingMultipler = PublicRatingFormatter.Default
                    };
            }

            movies.Clear();
        }

        private static bool SkipLines(int lineNumber)
        {
            if (lineNumber % 50000 == 1) Console.Write(".");
            return lineNumber < 380;
        }

        private static GenreModel ParseStringToModel(string line)
        {
            var movieName = line.Substring(0, line.IndexOf("\t", System.StringComparison.Ordinal)).Trim();
            var genre = line.Substring(movieName.Length + 1).Trim();

            return new GenreModel { Name = movieName, Genre = genre };
        }
    }
}