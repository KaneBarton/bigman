﻿using System.Collections.Generic;

namespace BigMan.Repository
{
    public interface IRepository<out T>
    {
        IEnumerable<T> Retrieve();
    }
}