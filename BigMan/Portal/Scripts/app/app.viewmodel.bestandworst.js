﻿var BestAndWorstViewModelHelper = {
    WireUpEvents: function () {
        window.bestAndWorstViewModel = {
            SearchesLoading: ko.observable(true),
            SearchesLoaded: ko.observable(false),
            Year: ko.observable(2013),
            BestResults: ko.observableArray(),
            WorstResults: ko.observableArray(),
        };
        
        ko.applyBindings(window.bestAndWorstViewModel, App.Section4[0]);
    }
};