﻿var TypeaheadHelper = {
    WireUpEvents: function () {
        App.MovieNameSearch.typeahead({
            name: 'movie_search',
            remote: '/api/typeahead?q=%QUERY',
            limit: 20,
        }).on('typeahead:autocompleted', TypeaheadHelper.OnSelected);
        //.on('typeahead:selected', TypeaheadHelper.OnAutocompleted)
    },

    OnAutocompleted: function () {
        console.log("OnAutocompleted");
        window.movieViewModel.SearchForMovie();
    },

    OnSelected: function () {
        //console.log("OnSelected");
        window.movieViewModel.SearchForMovie();
    }
};