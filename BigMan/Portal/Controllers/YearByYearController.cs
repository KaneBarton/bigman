﻿using System.Collections.Generic;
using System.Web.Http;
using BigMan.Portal.Models;
using BigMan.Services.Interfaces;

namespace BigMan.Portal.Controllers
{
    public class YearByYearController : ApiController
    {
        private readonly IYearByYearService _service;

        public YearByYearController(IYearByYearService service)
        {
            _service = service;
        }

        public IEnumerable<MovieBasicViewModel> Get()
        {
            return _service.Retrieve();
        }
    }
}
