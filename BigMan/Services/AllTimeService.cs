﻿using System.Collections.Generic;
using System.Linq;
using BigMan.Annotations;
using BigMan.Domain;
using BigMan.Portal.Models;
using BigMan.Repository;
using BigMan.Services.Interfaces;

namespace BigMan.Services
{
    [UsedImplicitly]
    public class AllTimeService : IAllTimeService
    {
        private readonly IMovieInformationService _movieInformationService;
        private readonly IRepository<Movie> _movieRepository;
        private static readonly IList<MovieViewModel> AllTimeList = new List<MovieViewModel>();

        public AllTimeService(IMovieInformationService movieInformationService, IRepository<Movie> movieRepository)
        {
            _movieInformationService = movieInformationService;
            _movieRepository = movieRepository;
        }

        public IEnumerable<MovieViewModel> Retrieve()
        {
            if (AllTimeList.Count != 0) return AllTimeList;

            var movies = _movieRepository.Retrieve().OrderByDescending(x => x.Score()).ThenByDescending(x => x.Year).ThenByDescending(x => x.NameUpper).Take(10).ToList();
            foreach (var domainModel in movies)
            {
                var movieInformation = _movieInformationService.Retrieve(domainModel);

                var viewModel = domainModel.MapToViewModel();
                viewModel.Plot = movieInformation.Plot;
                viewModel.ImageUrl = movieInformation.ImageUrl;

                AllTimeList.Add(viewModel);
            }

            return AllTimeList;
        }
    }
}