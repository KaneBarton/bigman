﻿using BigMan.Domain;

namespace BigMan.Services.Interfaces
{
    public interface IIMDBLookup
    {
        MovieInformation Retrieve(string imdbIdentifier);
    }
}