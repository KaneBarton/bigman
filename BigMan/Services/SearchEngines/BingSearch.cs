﻿using System;
using System.Linq;
using System.Net;
using System.Web;
using BigMan.Annotations;
using BigMan.Services.Interfaces;
using Bing;

namespace BigMan.Services.SearchEngines
{
    [UsedImplicitly]
    internal class BingSearch : IBingSearch
    {
        // Replace this value with your account key.
        private const string AccountKey = "mmXWzD1jHfpS/7vP7G3GhWcjp2p0iDLSTl1gVC503zQ=";
        private const string RootUrl = "https://api.datamarket.azure.com/Bing/Search";
        private const string QueryUrlWeb = "https://api.datamarket.azure.com/Bing/Search/v1/Web?Query='";
        private const string EndQuery = "'&Market='en-US'";
        private static readonly BingSearchContainer BingContainer = new global::Bing.BingSearchContainer(new Uri(RootUrl)) {  Credentials = new NetworkCredential(AccountKey, AccountKey) };

        public WebResult SearchWeb(string searchFor)
        {
            // This is the query expression.
            var searchQuery = HttpUtility.UrlEncode(searchFor + " imdb.com");
            var searchUri = new Uri(string.Concat(QueryUrlWeb, searchQuery, EndQuery));

            var webResults = BingContainer.Execute<WebResult>(searchUri);

            return webResults == null ? null : webResults.FirstOrDefault();
        }
    }
}