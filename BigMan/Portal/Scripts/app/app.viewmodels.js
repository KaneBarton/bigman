﻿var ModelHelper = {
    WireUpEvents: function () {
        MovieViewModelHelper.WireUpEvents();
        BestAndWorstViewModelHelper.WireUpEvents();
        YearByYearViewModelHelper.WireUpEvents();
        AllTimeViewModelHelper.WireUpEvents();
    }
}