﻿using BigMan.Annotations;
using BigMan.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BigMan.Tests.Domain
{
    [UsedImplicitly]
    public partial class MovieInformationTests
    {
        [TestClass]
        public class MovieName
        {
            [TestMethod]
            public void ReturnsUppercaseName()
            {
                // Arrange
                var domain = new MovieInformation {MovieName = "name"};

                // Act and Assert
                Assert.AreEqual("NAME", domain.MovieName);
            }
        }
    }
}
