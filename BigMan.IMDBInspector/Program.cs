﻿using System.Runtime.InteropServices;
using BigMan.Domain;
using BigMan.IMDBInspector.Formatters;
using BigMan.IMDBInspector.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace BigMan.IMDBInspector
{
    static class Program
    {
        private static readonly IDictionary<string, Movie> Movies = new Dictionary<string, Movie>();

        internal static string BasePath { get; private set; }

        static void Main()
        {
            Program.ReadMe();
            Console.WriteLine("Enter the root path of the IMDB *.list files>");

            BasePath = Console.ReadLine();
            Console.WriteLine();

            if (string.IsNullOrEmpty(BasePath)) return;

            if (!BasePath.EndsWith(@"\"))
            {
                BasePath += @"\";
            }

            var formatters = new List<IDataFormatter>(new IDataFormatter[]
                {
                    new YearFormatter(),
                    new AdvisoryRatingFormatter(), 
                    new PublicRatingFormatter(), 
                    new KeywordFormatter() 
                });

            var genreFormatter = new GenreFormatter();
            Console.WriteLine(genreFormatter.ToString());
            foreach (var movie in genreFormatter.Parse())
            {
                Movies.Add(movie.Name, movie);
            }

            foreach (var formatter in formatters)
            {
                Console.WriteLine();
                Console.WriteLine(formatter.ToString());

                foreach (var movie in formatter.Parse().Where(movie => Movies.ContainsKey(movie.Name)))
                {
                    Movies[movie.Name] = formatter.ApplyValues(Movies[movie.Name]);
                }
            }

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Parsing complete, now saving data");

            // remove all non-1970
            var moviesToRemove = Movies.Values.Where(x => x.Year <= Defaults.MinYear).Select(x => x.Name).ToArray();
            foreach (var movieName in moviesToRemove)
            {
                Movies.Remove(movieName);
            }

            var max = Movies.Values.Max(x => x.ManScore);

            var outputBuilder = new StringBuilder();
            foreach (var movie in Movies.Values.Where(x => x.IsDataComplete()))
            {
                movie.MaxManScore = (int)max;
                outputBuilder.AppendLine(movie.ToExportFormat());
            }

            File.WriteAllText(Path.Combine(BasePath, "MovieData.txt"), outputBuilder.ToString());

            outputBuilder = new StringBuilder();
            outputBuilder.AppendLine(new Movie().ToExportAppStudioExportFormat(true));
            foreach (var movie in Movies.Values.Where(x => x.IsDataComplete()).OrderByDescending(x => x.Score()).Take(40))
            {
                movie.MaxManScore = (int)max;
                outputBuilder.AppendLine(movie.ToExportAppStudioExportFormat());
            }

            File.WriteAllText(Path.Combine(BasePath, "AppStudio-MovieData.csv"), outputBuilder.ToString());

            Console.WriteLine();
            Console.WriteLine("File saved to {0} and the path has been copied to your clipboard", Path.Combine(BasePath, "MovieData.txt"));

            Program.SetClipboardText(BasePath);

            Console.Read();
        }

        #region Readme

        public static void ReadMe()
        {

            using (var stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("BigMan.IMDBInspector.readme.txt"))
            {
                if (stream != null)
                {
                    using (var streamReader = new StreamReader(stream))
                    {
                        var readMeMessage = streamReader.ReadToEnd();
                        Console.WriteLine(readMeMessage);
                    }
                }
            }
        }

        #endregion

        #region Clipboard Access
        // http://stackoverflow.com/questions/13571426/how-can-i-copy-a-string-to-clipboard-within-my-console-app-without-adding-a-refe

        [DllImport("user32.dll")]
        private static extern bool OpenClipboard(IntPtr hWndNewOwner);

        [DllImport("user32.dll")]
        private static extern bool CloseClipboard();

        [DllImport("user32.dll")]
        private static extern bool SetClipboardData(uint uFormat, IntPtr data);

        private static void SetClipboardText(string text)
        {
            OpenClipboard(IntPtr.Zero);
            var ptr = Marshal.StringToHGlobalUni(text);
            SetClipboardData(13, ptr);
            CloseClipboard();
            Marshal.FreeHGlobal(ptr);
        }
        #endregion
    }
}
