﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using BigMan.Annotations;
using BigMan.Domain;
using BigMan.Portal.Models;
using BigMan.Repository;
using BigMan.Services.Interfaces;

namespace BigMan.Services
{
    [UsedImplicitly]
    public class BestAndWorstService : IBestAndWorstService
    {
        private readonly IMovieInformationService _movieInformationService;
        private readonly IServiceCache<BestAndWorstViewModel> _serviceCache;
        private readonly IRepository<Movie> _movieRepository;

        public BestAndWorstService(IMovieInformationService movieInformationService, IServiceCache<BestAndWorstViewModel> serviceCache, IRepository<Movie> movieRepository)
        {
            _movieInformationService = movieInformationService;
            _serviceCache = serviceCache;
            _movieRepository = movieRepository;
        }

        public BestAndWorstViewModel Retrieve(int year)
        {
            if (_serviceCache.ContainsByKey(year.ToString(CultureInfo.InvariantCulture))) return _serviceCache.ItemByKey(year.ToString(CultureInfo.InvariantCulture));

            var model = new BestAndWorstViewModel
            {
                Year = year,

                Worst = _movieRepository.Retrieve().Where(x => x.Year.Equals(year))
                        .OrderBy(x => x.Score())
                        .ThenBy(x => x.Name)
                        .Take(5)
                        .Select(x => new MovieBasicViewModel
                        {
                            Name = x.Name,
                            Year = x.Year,
                            Score = x.Score()
                        }).ToArray(),

                Best = _movieRepository.Retrieve().Where(x => x.Year.Equals(year))
                            .OrderByDescending(x => x.Score())
                            .ThenBy(x => x.Name)
                            .Take(5)
                            .Select(x => new MovieBasicViewModel
                            {
                                Name = x.Name,
                                Year = x.Year,
                                Score = x.Score()
                            }).ToArray()
            };

            Parallel.Invoke(() => PopulateMovieInformation(model.Worst), () => PopulateMovieInformation(model.Best));

            _serviceCache.Add(model);

            return _serviceCache.ItemByKey(year.ToString(CultureInfo.InvariantCulture));
        }

        private void PopulateMovieInformation(IEnumerable<MovieBasicViewModel> data)
        {
            Parallel.ForEach(data, viewModel =>
            {
                viewModel.ImageUrl = _movieInformationService.Retrieve(viewModel.Name, viewModel.Year).ImageUrl;
                if (viewModel.ImageUrl == "N/A")
                {
                    viewModel.ImageUrl = Defaults.DefaultMovieInformation().ImageUrl;
                }
            });
        }
    }
}