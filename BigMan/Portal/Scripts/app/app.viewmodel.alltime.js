﻿var AllTimeViewModelHelper = {    
    WireUpEvents: function () {
        window.allTimeViewModel = {
            SearchesLoading: ko.observable(true),
            SearchesLoaded: ko.observable(false),
            Results: ko.observableArray()
        };

        ko.applyBindings(window.allTimeViewModel, App.Section3[0]);
    }
};