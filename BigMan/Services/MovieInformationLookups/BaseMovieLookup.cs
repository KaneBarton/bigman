﻿using System;
using System.Net;
using BigMan.Domain;
using BigMan.Extensions;
using BigMan.Services.Interfaces;

namespace BigMan.Services.MovieInformationLookups
{
    public abstract class BaseMovieLookup : IMovieInformationLookup
    {
        protected string MovieName { get; set; }

        public abstract int OrderSequence { get; }

        public MovieInformation Retrieve(string movieName, int year)
        {
            this.MovieName = movieName;

            // ReSharper disable RedundantAssignment
            var movieInformation = new MovieInformation { MovieName = movieName };
            // ReSharper restore RedundantAssignment

            try
            {
                // check local
                var local = LocalStorage.GetMovieInformation(movieInformation);
                if (!string.IsNullOrWhiteSpace(local))
                {
                    return MovieInformation.FromString(local);
                }

                var jsonResponse = GetResponse(movieName, year);

                if (TryParseResponse(jsonResponse, out movieInformation))
                {
                    movieInformation.MovieName = movieName;

                    // cache it locally so we don't ever have to do another server request
                    LocalStorage.StoreMovieInformation(movieInformation);

                    return movieInformation;
                }

                return null;
            }
            catch (AggregateException)
            {
                return Defaults.DefaultMovieInformation();
            }
            catch (WebException)
            {
                return Defaults.DefaultMovieInformation();
            }
        }

        private string GetResponse(string movieName, int year)
        {
            try
            {
                return WebRequestExtensions.HttpGet(BuildAPIUrl(movieName, year));
            }
            catch (WebException)
            {
                return string.Empty;
            }
        }

        public abstract bool TryParseResponse(string jsonResponse, out MovieInformation movieInformation);
        protected abstract string BuildAPIUrl(string movieName, int year);
    }
}