﻿namespace BigMan.Portal.Models
{
    public class MovieViewModel
    {
        public string Name { get; set; }
        public string Plot { get; set; }
        public string NameUpper { get; set; }
        public string ManTag { get; set; }
        public int Year { get; set; }
        public int Score { get; set; }
        public string ImageUrl { get; set; }
    }
}