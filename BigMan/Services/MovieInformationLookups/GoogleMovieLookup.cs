﻿using BigMan.Annotations;
using BigMan.Domain;
using BigMan.Extensions;
using BigMan.Services.Interfaces;

namespace BigMan.Services.MovieInformationLookups
{
    [UsedImplicitly]
    public class GoogleMovieLookup : BaseMovieLookup
    {
        private readonly IGoogleSearch _searchEngine;
        private readonly IIMDBLookup _imdbLookup;

        public GoogleMovieLookup(IGoogleSearch searchEngine, IIMDBLookup imdbLookup)
        {
            _searchEngine = searchEngine;
            _imdbLookup = imdbLookup;
        }

        public override int OrderSequence
        {
            get { return 4; }
        }

        public override bool TryParseResponse(string jsonResponse, out MovieInformation movieInformation)
        {
            movieInformation = new MovieInformation();

            // hit bing's API to get the IMDB movie number
            var googleSearch = _searchEngine.SearchWeb(MovieName);

            if (googleSearch != null)
            {
                string imdbId;
                if (WebRequestExtensions.TryParseImdbId(googleSearch.Link, out imdbId))
                {
                    movieInformation = _imdbLookup.Retrieve(imdbId);
                    return true;
                }
            }

            return false;
        }

        protected override string BuildAPIUrl(string movieName, int year)
        {
            return string.Empty;
        }
    }
}