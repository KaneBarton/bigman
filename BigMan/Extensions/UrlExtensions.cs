﻿using System;
using System.Web;

namespace BigMan.Extensions
{
    public static class UrlExtensions
    {
        public static string BaseUrl(HttpRequestBase url)
        {
            if (url == null || url.Url == null)
            {
                return string.Empty;
            }

            return string.Format("{0}://{1}/", url.Url.Scheme, url.Url.Authority);
        }

        public static string ControllerActionUrl(HttpRequestBase url, Type controller, string action)
        {
            return string.Format("{0}{1}/{2}/", BaseUrl(url), controller.Name.Substring(0, controller.Name.Length - 10), action);
        }
    }
}