﻿var WaypointHelper = {
    Htmlbody: $('html,body'),

    WireUpEvents: function () {
        var links = $('#nav').find('li');
        var moreButton = $('.intro button');
        var section = $('section');
        var mywindow = $(window);

        section.waypoint(function (direction) {
            var datasection = $(this).attr('data-section');
            if (direction === 'down') {
                App.NavDataSection[datasection].addClass('active').siblings().removeClass('active');
            }
            else {
                var newsection = parseInt(datasection) - 1;
                App.NavDataSection[newsection].addClass('active').siblings().removeClass('active');
            }
        });

        mywindow.scroll(function () {
            if (mywindow.scrollTop() == 0) {
                App.NavSection1.addClass('active');
                App.NavSection2.removeClass('active');
            }
        });

        function goToByScroll(datasection) {
            if (datasection == 1) {
                WaypointHelper.Htmlbody.animate({
                    scrollTop: App.DataSection[datasection].offset().top
                }, 750, 'easeOutQuart');
            }
            else {
                WaypointHelper.ScrollTo(datasection);
            }
        }

        links.click(function (e) {
            e.preventDefault();
            var datasection = $(this).attr('data-section');
            goToByScroll(datasection);
        });

        moreButton.click(function (e) {
            e.preventDefault();
            var datasection = $(this).attr('data-section');
            goToByScroll(datasection);
        });
    },
    
    ScrollTo: function(dataSection) {
        WaypointHelper.Htmlbody.animate({
            scrollTop: App.DataSection[dataSection].offset().top + 40
        }, 750, 'easeOutQuart');

    }
};