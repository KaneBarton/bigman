﻿using BigMan.Portal.Models;
using Newtonsoft.Json;
using System;

namespace BigMan.Domain
{
    public class Movie
    {
        private int _score;

        public Movie() : this(string.Empty) { }
        public Movie(string name, int maxScore = Defaults.MaxManScore)
        {
            KeywordsMultipler = 1;
            GenreMultiplier = 1;
            AdvisoryRatingMultipler = 1;
            MaxManScore = maxScore;
            Name = name;
            NameUpper = name.ToUpperInvariant();
        }

        // Basic properties
        public string Name { get; set; }
        public string NameUpper { get; set; }
        public virtual int Year { get; set; }

        // ManScore calculation fields
        public virtual int MaxManScore { get; set; }
        public virtual int GenreMultiplier { get; set; }
        public virtual int AdvisoryRatingMultipler { get; set; }
        public virtual int KeywordsMultipler { get; set; }
        public virtual int PublicRatingMultipler { get; set; }

        public virtual decimal ManScore
        {
            get { return GenreMultiplier * AdvisoryRatingMultipler * KeywordsMultipler * PublicRatingMultipler; }
        }

        public int Score()
        {
            if (_score == 0 && MaxManScore > 0 && ManScore > 0)
            {
                _score = (int)(Math.Round(ManScore / MaxManScore, 2) * 100);
            }

            if (_score == 0) _score = 1;

            return _score;
        }

        public bool IsDataComplete()
        {
            return Year > Defaults.MinYear &&
                   AdvisoryRatingMultipler > 0 &&
                   GenreMultiplier > 0 &&
                   KeywordsMultipler > 0 &&
                   PublicRatingMultipler > 0;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public string ToExportFormat()
        {
            return string.Format("{1}{0}{2}{0}{3}{0}{4}{0}{5}{0}{6}{0}{7}", "\t", Name, Year, MaxManScore, GenreMultiplier, AdvisoryRatingMultipler, KeywordsMultipler, PublicRatingMultipler);
        }


        public string ToExportAppStudioExportFormat(bool header = false)
        {
            if (header)
            {
                return "Name;Year;Score";                
            }

            return string.Format("{1}{0}{2}{0}scoring {3} of 100", ";", Name, Year, Score());
        }

        public MovieViewModel MapToViewModel()
        {
            return new MovieViewModel
            {
                Name = this.Name,
                NameUpper = this.NameUpper,
                Year = this.Year,
                Score = this.Score(),
            };
        }

        public static Movie FromExportFormat(string data)
        {
            var parsedData = data.Split('\t');
            return new Movie
                {
                    Name = parsedData[0],
                    NameUpper = parsedData[0].ToUpperInvariant(),
                    Year = int.Parse(parsedData[1]),
                    MaxManScore = int.Parse(parsedData[2]),
                    GenreMultiplier = int.Parse(parsedData[3]),
                    AdvisoryRatingMultipler = int.Parse(parsedData[4]),
                    KeywordsMultipler = int.Parse(parsedData[5]),
                    PublicRatingMultipler = int.Parse(parsedData[6])
                };
        }

        public bool IsDefault()
        {
            return Name == Defaults.MovieName;
        }
    }
}