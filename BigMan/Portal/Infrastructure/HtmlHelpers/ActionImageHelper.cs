﻿using System.Globalization;
using System.Web.Mvc;
using System.Web.Routing;

namespace BigMan.Portal.Infrastructure.HtmlHelpers
{
    public static class ActionImageHelper
    {
        public static MvcHtmlString ActionImage(this HtmlHelper html, string action, string imagePath, string alt, RouteValueDictionary routeValues = null, string cssClass = null, int width = 0, int height = 0)
        {
            var url = new UrlHelper(html.ViewContext.RequestContext);

            // build the <img> tag
            var imgBuilder = new TagBuilder("img");
            imgBuilder.MergeAttribute("src", url.Content(imagePath));
            imgBuilder.MergeAttribute("alt", alt);
            imgBuilder.MergeAttribute("title", alt);
            if (!width.Equals(0)) imgBuilder.MergeAttribute("width", width.ToString(CultureInfo.InvariantCulture));
            if (!height.Equals(0)) imgBuilder.MergeAttribute("height", height.ToString(CultureInfo.InvariantCulture));
            var imgHtml = imgBuilder.ToString(TagRenderMode.SelfClosing);

            // build the <a> tag
            var anchorBuilder = new TagBuilder("a");
            anchorBuilder.MergeAttribute("href", url.Action(action, routeValues));
            anchorBuilder.MergeAttribute("title", "Bestimate");


            if (!string.IsNullOrWhiteSpace(cssClass))
            {
                anchorBuilder.AddCssClass(cssClass);
            }

            anchorBuilder.InnerHtml = imgHtml; // include the <img> tag inside
            var anchorHtml = anchorBuilder.ToString(TagRenderMode.Normal);

            return MvcHtmlString.Create(anchorHtml);
        }
    }
}