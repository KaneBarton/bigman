100	Balls to the wall action!
100	I came. I saw. I conquered!
100	Are you not entertained! Are you not entertained! Is this not why you are here!
99	It doesn't get any better than this	
99	It's better to die on your feet than to live on your knees
98	You'll never get a Purple Heart hiding in a foxhole! Follow me!
98	Do I feel lucky?' Well, do ya, punk?
97	Damn the torpedoes! Full speed ahead!
97	What we do in life echoes in eternity
96	One man with courage makes a majority!
95	If you kill enough of them, they stop fighting
94	Don't fire unless fired upon, but if they mean to have a war, let it begin here
93	Damn the torpedoes! Full speed ahead!
92	We shall have our fight in the shade!
91	Nothing splendid has ever been achieved except by those who dared believe that something inside them was superior to circumstance.
91	Machete don't text.
91	Oh, behave! Yeah! Yeah, baby!
90	Say 'hello' to my little friend!
90	Ssssmmokin'!
89	After all, tomorrow is another day!
89	It's only after we've lost everything that we're free to do anything
88	Carpe diem. Seize the day, boys. Make your lives extraordinary.
87	Go ahead, make my day.
86	Hasta la vista, baby.
86	Oooh! Somebody stop me!
85	May the Force be with you.
84	I feel the need�the need for speed!
83	I love the smell of napalm in the morning.
82	I'm as mad as hell, and I'm not going to take this anymore!
81	Keep your friends close, but your enemies closer.
80	Life is a banquet, and most poor suckers are starving to death!
79	Mama always said life was like a box of chocolates. You never know what you're gonna get.
78	Round up the usual suspects.
77	You talkin' to me?
77	Mmmm-hmmm! This is a tasty burger!
76	Do, or do not. There is no "try".
75	Life moves pretty fast. If you don't stop and look around once in a while, you could miss it.
74	If you're not willing to risk it all, then you don't want it bad enough
73	There's three ways to do things, the right way, the wrong way and the way that I do it.
72	Kid, there are heroes and there are legends. Heroes get remembered, but legends never die
71	The brave do not live forever but the cautious do not live at all.
70	I'm sorry, did I break your concentration?
69	I find your lack of faith disturbing
68	We're gonna be doin' one thing and one thing only... killin' Nazis.
67	I just added two more guys to my wolf pack
67	Now, you got a corpse in a car minus a head in the garage. Take me to it.
66	Heres to lying, stealing, cheating, and drinking.
66	Want to know how I got these scars? My father was a drinker and a fiend. And one night, he goes off crazier than usual. Mommy gets the kitchen knife to defend herself. He doesn't like that. Not one bit. So, me watching, he takes the knife to her, laughing while he does it. He turns to me, and he says: 'Why so serious?' He comes at me with the knife - 'Why so serious?!' He sticks the blade in my mouth. 'Let's put a smile on that face!' And why so serious?
65	A census taker once tried to test me. I ate his liver with some fava beans and a nice chianti
64	As far back as I can remember, I always wanted to be a gangster
63	Look, you shoot off a guy's head with his pants down, believe me, Texas ain't the place you want to get caught.
62	First you want to kill me. Now you want to kiss me. Blow!
61	It's a hell of a thing, killin' a man. You take away all he's got and all he's ever gonna have
60	Ernest Hemingway once wrote: 'The world is a fine place and worth fighting for.' I agree with the second part.
59	Everything in this room is eatable. Yes, I am eatable but that, children is called cannibelism and is frowned upon in most societies
58	Vanity is definitely my favorite sin
57	You know what the difference is between you and me? I make this look GOOD
56	Because sometimes the truth isn't good enough, people deserve a little more.
55	Guys out there is hurted
54	Oh yeah, you do what you gotta do
53	Me, I always tell the truth, even when I lie
52	Some folks call it a sling blade. I call it a kaiser blade.
51	At my signal, unleash hell
51	...Father to a murdered son. Husband to a murdered wife. And I will have my vengeance, in this life or the next
50	Aaaaawl-right-y-then
49	Should you decide to defuse a bomb, don't worry which wire to cut. You will always choose the right one.
48	Well, what if there is no tomorrow? There wasn't one today
47	Always look on the bright side of life
46	Wax on... wax off. Wax on... wax off
45	Never tell me the odds.
44	This is your life and it's ending one minute at a time
43	It's only after we've lost everything that we're free to do anything
42	Don't give up the ship! Fight her till she sinks!
41	This is blasphemy! This is madness! ... Madness? This is Sparta!
40	Somebody help me, I'm being spontaneous!
39	What we've got here is failure to communicate.
38	I feel like I'm taking crazy pills!
37	Our Lives are defined by moments, especially the ones we never see coming.
36	It's better to have something and not need it than to need something and not have it.
36	Ernest Hemingway once wrote: 'The world is a fine place and worth fighting for.' I agree with the second part
35	Define 'irony'. A bunch of idiots dancing on a plane to a song made famous by a band that died in a plane crash.
35	You keep on knocking on the devil's door, eventually someone will answer.
34	If you're good at something never do it for free
33	Fear is the path to the dark side. Fear leads to anger. Anger leads to hate. Hate leads to suffering.
32	I feel like an idiot. But I am an idiot, so it kinda works out.
31	Hello, my name is Inigo Montoya. You killed my father. Prepare to die!
31	The greatest trick the devil ever pulled was convincing the world he didn't exist. And like that - he's gone.
31	I know who I am! I'm a dude playing a dude disguised as another dude!
30	Do us a favor... I know it's difficult for you... but please, stay here, and try not to do anything... stupid.
29	The only true wisdom consists of knowing that you know nothing.
28	You are without doubt the worst pirate I've ever heard of.
28	I have been stabbed, shot, poisoned, frozen, hung, electrocuted, and burned
27	Do you ever wish that sometimes you could freezeframe a moment in your day look at it and say 'this is not my life.'?
26	One of these days you're going to have the chance to do something right!
25	We can stay up all night swapping manly stories and in the morning, I'm making waffles!
24	That's just the trouble with me, I give myself very good advice, but I very seldom follow it.
23	Thats what people do. They leap, and hope to God they can fly.
23	Well, Clarice, have the lambs stopped screaming?
22	If someone took everything you live for... How far would you go to get it back?
22	You got knocked the f--k out!
21	Nothing, I got nothing, seriously
20	Get busy livin' or get busy dyin'. (That's goddamn right.)
19	Every story has an end, but in life every ending is just a new begining.
18	I live in a world full of people pretending to be something they're not
17	Never let the fear of striking out keep you from playing the game!
16	Some memories are best forgotten
15	Why are you trying so hard to fit in when you were born to stand out?
15	I don't speak freaky deeky Dutch ok?
14	Show me the money!
14	The muppets made a better movie than this
13	Fasten your seatbelts. It's going to be a bumpy night.
12	You can't handle the truth!
11	You're gonna need a bigger boat.
10	You've got to ask yourself one question: 'Do I feel lucky?' Well, do ya, punk?
10	Get off my plane.
9	Houston, we have a problem.
9	There are two people who I trust, the first person is me the other is not you!
8	Wise men speak because they have something to say; Fools because they have to say something.
7	Do what is right because it is right, and leave it alone.
7	Don't give up your day job
6	I am just going outside. I may be some time.
6	Stupid is as stupid does.
5	This is a good day to die!
5	Scratch my eyes out
5	I crap bigger than you!
4	Take your stinking paws off me, you damned dirty ape!
3	Your time is limited, so don't waste it living someone else's life
3	I could ask for your opinion but I really don't care that much about you
2	You're a sad, strange little man, and you have my pity
1	Go and cuddle your teddy bear 
1	This blows
1	Why bother
1	Mama says, 'Stupid is as stupid does
1	My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.
1	Dave, this conversation can serve no purpose anymore. Goodbye.
1	Goodbye, Mr. Bond.
1	Fuh-get about it!
0	