﻿namespace BigMan.Portal.Models.HubModels
{
    public class RecentlySearchedHubModel
    {
        public string ConnectionId { get; set; }
        public string MovieName { get; set; }
        public int Year { get; set; }
    }
}