﻿namespace BigMan.IMDBInspector.Models
{
    internal class GenreModel
    {
        internal static string[] RemovedGenres = { };

        internal string Name { get; set; }
        internal string Genre { get; set; }

        internal bool NameIsValid
        {
            get
            {
                return !(Name.EndsWith("(TV)") || Name.EndsWith("(VG)") || Name.Contains("{") || Name.Contains("}") ||
                        Name.EndsWith("(V)") || Name.StartsWith("\""));
            }
        }

        internal int Score()
        {
            switch (Genre.ToUpperInvariant())
            {
                case "ACTION": return 18;
                case "WESTERN": return 17;
                case "HORROR": return 16;
                case "THRILLER": return 15;
                case "CRIME": return 14;
                case "SPORT": return 13;
                case "WAR": return 12;
                case "SCI-FI": return 11;
                case "MYSTERY": return 10;
                case "HISTORY": return 9;
                case "COMEDY": return 8;
                case "ANIMATION": return 7;
                case "FANTASY": return 6;
                case "ADVENTURE": return 5;
                case "DRAMA": return 4;
                case "FAMILY": return 3;
                case "ROMANCE": return 1;
                default: return 0;
            }
        }
    }
}