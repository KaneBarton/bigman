﻿namespace BigMan.Domain
{
    public class ManTag
    {
        public int Score { get; set; }
        public string Tag { get; set; }
    }
}