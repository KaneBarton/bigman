﻿using Microsoft.AspNet.SignalR;
using System;
using System.IO;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace BigMan.Portal.Infrastructure.ReatimeCollaboration
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public class HubAttribute : ActionFilterAttribute
    {
        public string Name { get; set; }
        public string IncomingMethod { get; set; }
        public string OutgoingMethod { get; set; }

        public override async void OnActionExecuting(HttpActionContext actionContext)
        {
            if (string.IsNullOrWhiteSpace(Name) || string.IsNullOrWhiteSpace(IncomingMethod)) return;
            var hub = GlobalHost.ConnectionManager.GetHubContext(Name);

            if (hub != null)
            {
                var text = string.Empty;
                var stream = await actionContext.Request.Content.ReadAsStreamAsync();
                if (stream.CanSeek)
                {
                    stream.Position = 0;
                    using (stream)
                    {
                        var reader = new StreamReader(stream);
                        text = reader.ReadToEnd();
                    }
                }

                hub.Clients.All.Invoke(IncomingMethod,
                       new
                       {
                           Time = DateTime.Now.ToString("G"),
                           Data = actionContext.Request.Method + " " + actionContext.Request.RequestUri +
                       " " + text
                       });
            }
        }

        public override async void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            if (string.IsNullOrWhiteSpace(Name) || string.IsNullOrWhiteSpace(OutgoingMethod)) return;
            var hub = GlobalHost.ConnectionManager.GetHubContext(Name);

            if (hub == null) return;
            var response = await actionExecutedContext.Response.Content.ReadAsAsync(actionExecutedContext.ActionContext.ActionDescriptor.ReturnType);
            hub.Clients.All.Invoke(OutgoingMethod, new { Time = DateTime.Now.ToString("G"), Data = response });
        }
    }
}