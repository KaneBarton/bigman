﻿using BigMan.Annotations;
using BigMan.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BigMan.Tests.Services.MyMovieLookup
{
    [UsedImplicitly]
    public partial class MyMovieLookupTests
    {
        [TestClass]
        public class TryParseResponse
        {
            [TestMethod]
            public void WhenEmptyResponseThenNotValid()
            {
                // Arrange
                var lookup = new BigMan.Services.MovieInformationLookups.MyMovieLookup();
                MovieInformation movieInformation;
                 
                // Act
                var isValid = lookup.TryParseResponse(string.Empty, out movieInformation);

                // Assert
                Assert.IsFalse(isValid);
            }

            [TestMethod]
            public void WhenResponseContainsCodeElementThenNotValid()
            {
                // Arrange
                var lookup = new BigMan.Services.MovieInformationLookups.MyMovieLookup();
                MovieInformation movieInformation;

                // Act
                var isValid503 = lookup.TryParseResponse("{\"code\":503, \"error\":\"API limit exceeded\"}", out movieInformation);
                var isValid404 = lookup.TryParseResponse("{\"code\":404, \"error\":\"Film not found\"}", out movieInformation);

                // Assert
                Assert.IsFalse(isValid503);
                Assert.IsFalse(isValid404);
            }

            [TestMethod]
            public void WhenResponseDoesNotContainPlotThenInValid()
            {
                // Arrange
                var lookup = new BigMan.Services.MovieInformationLookups.MyMovieLookup();
                MovieInformation movieInformation;

                // Act
                var isValid = lookup.TryParseResponse("{\"genres\": [\"Action\", \"Fantasy\", \"History\", \"War\"], \"rating\": 7.7, \"language\": [\"English\"], \"rating_count\": 410972, \"country\": [\"USA\"], \"release_date\": 20070314, \"title\": \"300\", \"year\": 2006, \"filming_locations\": \"Icestorm Studios, Montr\u00e9al, Qu\u00e9bec, Canada\", \"imdb_id\": \"tt0416449\", \"directors\": [\"Zack Snyder\"], \"writers\": [\"Zack Snyder\", \"Kurt Johnstad\"], \"actors\": [\"Gerard Butler\", \"Lena Headey\", \"Dominic West\", \"David Wenham\", \"Vincent Regan\", \"Michael Fassbender\", \"Tom Wisdom\", \"Andrew Pleavin\", \"Andrew Tiernan\", \"Rodrigo Santoro\", \"Giovani Cimmino\", \"Stephen McHattie\", \"Greg Kramer\", \"Alex Ivanovici\", \"Kelly Craig\"], \"plot_simple\": \"King Leonidas and a force of 300 men fight the Persians at Thermopylae in 480 B.C.\", \"poster\": {\"imdb\": \"http://ia.media-imdb.com/images/M/MV5BMjAzNTkzNjcxNl5BMl5BanBnXkFtZTYwNDA4NjE3._V1_SX214_.jpg\", \"cover\": \"http://imdb-poster.b0.upaiyun.com/000/416/449.jpg!cover?_upt=a551a7191380239282\"}, \"runtime\": [\"117 min\"], \"type\": \"M\", \"imdb_url\": \"http://www.imdb.com/title/tt0416449/\", \"also_known_as\": [\"300 - Bitka kod Termopila\"]}", out movieInformation);

                // Assert
                Assert.IsFalse(isValid);
            }

            [TestMethod]
            public void WhenResponseContainsPlotButNoPosterImageThenInValid()
            {
                // Arrange
                var lookup = new BigMan.Services.MovieInformationLookups.MyMovieLookup();
                MovieInformation movieInformation;

                // Act
                var isValid = lookup.TryParseResponse("{\"plot\": \"In the Battle of Thermopylae of 480 BC an alliance of Greek city-states fought the invading Persian army in the mountain pass of Thermopylae. Vastly outnumbered, the Greeks held back the enemy in one of the most famous last stands of history. Persian King Xerxes lead a Army of well over 100,000 (Persian king Xerxes before war has about 170,000 army) men to Greece and was confronted by 300 Spartans, and several hundred Arcadians. Xerxes waited for 10 days for King Leonidas to surrender or withdraw left with no options he moved. The battle lasted for about 3 days and after which all 300 Spartans were killed. The Spartan defeat was not the one expected, as a local shepherd, named Ephialtes, defected to the Persians and informed Xerxes of a separate path through Thermopylae, which the Persians could use to outflank the Greeks.\", \"genres\": [\"Action\", \"Fantasy\", \"History\", \"War\"], \"rating\": 7.7, \"language\": [\"English\"], \"rating_count\": 410972, \"country\": [\"USA\"], \"release_date\": 20070314, \"title\": \"300\", \"year\": 2006, \"filming_locations\": \"Icestorm Studios, Montr\u00e9al, Qu\u00e9bec, Canada\", \"imdb_id\": \"tt0416449\", \"directors\": [\"Zack Snyder\"], \"writers\": [\"Zack Snyder\", \"Kurt Johnstad\"], \"actors\": [\"Gerard Butler\", \"Lena Headey\", \"Dominic West\", \"David Wenham\", \"Vincent Regan\", \"Michael Fassbender\", \"Tom Wisdom\", \"Andrew Pleavin\", \"Andrew Tiernan\", \"Rodrigo Santoro\", \"Giovani Cimmino\", \"Stephen McHattie\", \"Greg Kramer\", \"Alex Ivanovici\", \"Kelly Craig\"], \"plot_simple\": \"King Leonidas and a force of 300 men fight the Persians at Thermopylae in 480 B.C.\", \"poster\": {}, \"runtime\": [\"117 min\"], \"type\": \"M\", \"imdb_url\": \"http://www.imdb.com/title/tt0416449/\", \"also_known_as\": [\"300 - Bitka kod Termopila\"]}", out movieInformation);

                // Assert
                Assert.IsFalse(isValid);
            }


            [TestMethod]
            public void WhenResponseContainsPlotAndImageThenValid()
            {
                // Arrange
                var lookup = new BigMan.Services.MovieInformationLookups.MyMovieLookup();
                MovieInformation movieInformation;

                // Act
                var isValid = lookup.TryParseResponse("{\"plot\": \"In the Battle of Thermopylae of 480 BC an alliance of Greek city-states fought the invading Persian army in the mountain pass of Thermopylae. Vastly outnumbered, the Greeks held back the enemy in one of the most famous last stands of history. Persian King Xerxes lead a Army of well over 100,000 (Persian king Xerxes before war has about 170,000 army) men to Greece and was confronted by 300 Spartans, and several hundred Arcadians. Xerxes waited for 10 days for King Leonidas to surrender or withdraw left with no options he moved. The battle lasted for about 3 days and after which all 300 Spartans were killed. The Spartan defeat was not the one expected, as a local shepherd, named Ephialtes, defected to the Persians and informed Xerxes of a separate path through Thermopylae, which the Persians could use to outflank the Greeks.\", \"genres\": [\"Action\", \"Fantasy\", \"History\", \"War\"], \"rating\": 7.7, \"language\": [\"English\"], \"rating_count\": 410972, \"country\": [\"USA\"], \"release_date\": 20070314, \"title\": \"300\", \"year\": 2006, \"filming_locations\": \"Icestorm Studios, Montr\u00e9al, Qu\u00e9bec, Canada\", \"imdb_id\": \"tt0416449\", \"directors\": [\"Zack Snyder\"], \"writers\": [\"Zack Snyder\", \"Kurt Johnstad\"], \"actors\": [\"Gerard Butler\", \"Lena Headey\", \"Dominic West\", \"David Wenham\", \"Vincent Regan\", \"Michael Fassbender\", \"Tom Wisdom\", \"Andrew Pleavin\", \"Andrew Tiernan\", \"Rodrigo Santoro\", \"Giovani Cimmino\", \"Stephen McHattie\", \"Greg Kramer\", \"Alex Ivanovici\", \"Kelly Craig\"], \"plot_simple\": \"King Leonidas and a force of 300 men fight the Persians at Thermopylae in 480 B.C.\", \"poster\": {\"imdb\": \"http://ia.media-imdb.com/images/M/MV5BMjAzNTkzNjcxNl5BMl5BanBnXkFtZTYwNDA4NjE3._V1_SX214_.jpg\", \"cover\": \"http://imdb-poster.b0.upaiyun.com/000/416/449.jpg!cover?_upt=a551a7191380239282\"}, \"runtime\": [\"117 min\"], \"type\": \"M\", \"imdb_url\": \"http://www.imdb.com/title/tt0416449/\", \"also_known_as\": [\"300 - Bitka kod Termopila\"]}", out movieInformation);

                // Assert
                Assert.IsTrue(isValid);
            }
        } 
    }
}
