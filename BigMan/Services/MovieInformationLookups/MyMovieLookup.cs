﻿using System.Web;
using BigMan.Annotations;
using BigMan.Domain;
using Newtonsoft.Json;

namespace BigMan.Services.MovieInformationLookups
{
    public class MyMovieLookup : BaseMovieLookup
    {
        public override int OrderSequence
        {
            get { return -1; }
        }

        public override bool TryParseResponse(string jsonResponse, out MovieInformation movieInformation)
        {
            movieInformation = new MovieInformation();

            if (string.IsNullOrWhiteSpace(jsonResponse)) return false;

            var movieData = JsonConvert.DeserializeObject<JMovie>(jsonResponse);
            if (movieData.IsValid())
            {
                movieInformation.Plot = movieData.Plot;
                movieInformation.ImageUrl = movieData.Poster.Imdb ?? movieData.Poster.Cover;
            }

            return movieData.IsValid();
        }

        protected override string BuildAPIUrl(string movieName, int year)
        {
            return string.Format("http://mymovieapi.com/?title={0}&type=json&plot=full&episode=1&limit=1&year={1}&yg=1&mt=M&lang=en-US&offset=&aka=simple&release=simple&business=0&tech=0", HttpUtility.UrlEncode(movieName), year);
        }

        [UsedImplicitly]
        private class JMovie
        {
            public string Plot { get; set; }
            public JPoster Poster { get; set; }

            public bool IsValid()
            {
                return !string.IsNullOrEmpty(Plot) && Poster != null && Poster.IsValid();
            }

            internal class JPoster
            {
                public string Cover { get; set; }
                public string Imdb { get; set; }

                public bool IsValid()
                {
                    return !(string.IsNullOrEmpty(Cover) && string.IsNullOrEmpty(Imdb));
                }
            }
        }
    }
}