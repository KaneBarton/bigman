namespace BigMan.IMDBInspector.Models
{
    internal class AdvisoryRatingModel
    {
        public static int Default = 5;

        internal string Name { get; set; }
        internal string Rating { get; set; }

        internal int Score()
        {
            switch (Rating.ToUpperInvariant())
            {
                case "R": return 6;
                case "NC-17": return 5;
                case "M": return 4;
                case "PG-13": return 3;
                case "PG": return 2;
                case "G": return 1;
                default: return 4;
            }
        }
    }
}