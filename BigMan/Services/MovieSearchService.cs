﻿using BigMan.Annotations;
using BigMan.Domain;
using BigMan.Repository;
using System.Collections.Generic;
using System.Linq;
using BigMan.Services.Interfaces;

namespace BigMan.Services
{
    [UsedImplicitly]
    public class MovieNameSearchService : IMovieNameSearchService
    {
        private readonly IRepository<Movie> _movieRepository;

        public MovieNameSearchService(IRepository<Movie> movieRepository)
        {
            _movieRepository = movieRepository;
        }

        public IEnumerable<string> Retrieve(string nameSearch)
        {
            var nameSearchUpper = nameSearch.ToUpperInvariant();
            return _movieRepository.Retrieve().Where(x => x.NameUpper.Contains(nameSearchUpper)).Select(x => x.Name).Take(20);
        }
    }
}