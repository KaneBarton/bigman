﻿====================================================================================================================================
This simple console application parses IMDB text files and calculates the manliness index for each movie.

To use this program you will need to download the IMDB alternative interface files.
Details on the usage of these files can be found at http://www.imdb.com/interfaces

The files can be downloaded from any of the following locations:
	ftp://ftp.fu-berlin.de/pub/misc/movies/database/
	ftp://ftp.funet.fi/pub/mirrors/ftp.imdb.com/pub/
	ftp://ftp.sunet.se/pub/tv+movies/imdb/

You will need to extract the following files to a local path on your computer (E.G., C:\Temp)
	genres.list
	keywords.list
	movies.list
	mpaa-ratings-reasons.list
	ratings.list 

Once the files have been parsed the output MovieData.txt file text needs to be placed in the App_Data folder of the website.
====================================================================================================================================
