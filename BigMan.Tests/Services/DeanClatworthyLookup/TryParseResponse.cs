﻿using BigMan.Annotations;
using BigMan.Domain;
using BigMan.Services.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BigMan.Tests.Services.DeanClatworthyLookup
{
    [UsedImplicitly]
    public partial class DeanClatworthyLookupTests
    {
        [TestClass]
        public class TryParseResponse
        {
            [TestMethod]
            public void WhenEmptyResponseThenNotValid()
            {
                // Arrange
                var imdbMock = new Mock<IIMDBLookup>();
                var lookup = new BigMan.Services.MovieInformationLookups.DeanClatworthyLookup(imdbMock.Object);
                MovieInformation movieInformation;

                // Act
                var isValid = lookup.TryParseResponse(string.Empty, out movieInformation);

                // Assert
                Assert.IsFalse(isValid);
            }

            [TestMethod]
            public void WhenResponseHasErrorElementThenNotValid()
            {
                // Arrange
                var imdbMock = new Mock<IIMDBLookup>();
                var lookup = new BigMan.Services.MovieInformationLookups.DeanClatworthyLookup(imdbMock.Object);
                MovieInformation movieInformation;

                // Act
                var isValid = lookup.TryParseResponse("code|1 error|Film not found", out movieInformation);

                // Assert
                Assert.IsFalse(isValid);
            }

            [TestMethod]
            public void WhenResponseDoesNotContainErrorThenValid()
            {
                // Arrange
                var imdbMock = new Mock<IIMDBLookup>();
                imdbMock.Setup(x => x.Retrieve(string.Empty)).Returns(Defaults.DefaultMovieInformation);
                
                var lookup = new BigMan.Services.MovieInformationLookups.DeanClatworthyLookup(imdbMock.Object);
                MovieInformation movieInformation;

                // Act
                var isValid = lookup.TryParseResponse("imdbid|tt0117731 imdburl|http://www.imdb.com/title/tt0117731/ genres|Action,Adventure,Sci-Fi,Thriller languages|English country|USA votes|75465 stv|0 series|0 rating|7.5 runtime|111min title|Star Trek: First Contact year|1996 usascreens|2812 ukscreens|0 cacheExpiry|1380947606", out movieInformation);

                // Assert
                Assert.IsTrue(isValid);
            }
        }
    }
}