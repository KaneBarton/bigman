﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace BigMan.Portal.Infrastructure.ReatimeCollaboration
{
    [HubName("bigmanActions")]
    public class BigManActionsHub : Hub
    {
    }
}