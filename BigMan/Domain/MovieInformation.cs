﻿using System.Text.RegularExpressions;

namespace BigMan.Domain
{
    public class MovieInformation
    {
        private string _movieName;

        public string ImageUrl { get; set; }
        public string ManTag { get; set; }
        public string Plot { get; set; }

        public string MovieName
        {
            get { return _movieName.ToUpperInvariant(); }
            set { _movieName = value; }
        }

        public string MovieSafeName
        {
            get
            {
                return Regex.Replace(MovieName, @"\W|_", "", RegexOptions.Singleline);
            }
        }

        public string FolderName
        {
            get
            {
                return Regex.Match(MovieName, "[A-Z0-9]").ToString();
            }
        }

        public override string ToString()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }

        public static MovieInformation FromString(string json)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<MovieInformation>(json);
        }
    }
}