﻿using BigMan.Annotations;
using BigMan.Domain;
using BigMan.Services.Interfaces;
using Bing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BigMan.Tests.Services.BingMovieLookup
{
    [UsedImplicitly]
    public partial class BingMovieLookupTests
    {
        [TestClass]
        public class TryParseResponse
        {
            [TestMethod]
            public void WhenNoSearchResultsThenNotValid()
            {
                // Arrange
                var imdbMock = new Mock<IIMDBLookup>();
                var searchEngineMock = new Mock<IBingSearch>();
                var lookup = new BigMan.Services.MovieInformationLookups.BingMovieLookup(searchEngineMock.Object, imdbMock.Object);
                MovieInformation movieInformation;

                searchEngineMock.Setup(x => x.SearchWeb(string.Empty)).Returns((WebResult)null);

                // Act
                var isValid = lookup.TryParseResponse(string.Empty, out movieInformation);

                // Assert
                Assert.IsFalse(isValid);
            }

            [TestMethod]
            public void WhenTheFirstSearchResultDoesNotContainAnIMDBLinkThenNotValid()
            {
                // Arrange
                var imdbMock = new Mock<IIMDBLookup>();
                var searchEngineMock = new Mock<IBingSearch>();
                var resultMock = new WebResult {DisplayUrl = "NOTHING USEFUL"};

                var lookup = new BigMan.Services.MovieInformationLookups.BingMovieLookup(searchEngineMock.Object, imdbMock.Object);
                MovieInformation movieInformation;

                searchEngineMock.Setup(x => x.SearchWeb(string.Empty)).Returns(resultMock);

                // Act
                var isValid = lookup.TryParseResponse(string.Empty, out movieInformation);

                // Assert
                Assert.IsFalse(isValid);
            }

            [TestMethod]
            public void WhenTheFirstSearchContainAnIMDBLinkThenValid()
            {
                // Arrange
                var imdbMock = new Mock<IIMDBLookup>();
                var searchEngineMock = new Mock<IBingSearch>();
                var searchResult = new WebResult { DisplayUrl = "http://www.imdb.com/title/tt0095016/" };
                
                var lookup = new BigMan.Services.MovieInformationLookups.BingMovieLookup(searchEngineMock.Object, imdbMock.Object);
                MovieInformation movieInformation;

                searchEngineMock.Setup(x => x.SearchWeb(null)).Returns(searchResult);

                // Act
                var isValid = lookup.TryParseResponse(null, out movieInformation);

                // Assert
                Assert.IsTrue(isValid);
            }
        }
    }
}