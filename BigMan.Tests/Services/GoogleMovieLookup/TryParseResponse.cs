﻿using BigMan.Annotations;
using BigMan.Domain;
using BigMan.Services.Interfaces;
using Google.Apis.Customsearch.v1.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BigMan.Tests.Services.GoogleMovieLookup
{
    [UsedImplicitly]
    public partial class GoogleMovieLookupTests
    {
        [TestClass]
        public class TryParseResponse
        {
            [TestMethod]
            public void WhenNoSearchResultsThenNotValid()
            {
                // Arrange
                var imdbMock = new Mock<IIMDBLookup>();
                var searchEngineMock = new Mock<IGoogleSearch>();
                var lookup = new BigMan.Services.MovieInformationLookups.GoogleMovieLookup(searchEngineMock.Object, imdbMock.Object);
                MovieInformation movieInformation;

                searchEngineMock.Setup(x => x.SearchWeb(string.Empty)).Returns((Result)null);

                // Act
                var isValid = lookup.TryParseResponse(string.Empty, out movieInformation);

                // Assert
                Assert.IsFalse(isValid);
            }


            [TestMethod]
            public void WhenTheFirstSearchResultDoesNotContainAnIMDBLinkThenNotValid()
            {
                // Arrange
                var imdbMock = new Mock<IIMDBLookup>();
                var searchEngineMock = new Mock<IGoogleSearch>();
                var resultMock = new Mock<Result>();

                var lookup = new BigMan.Services.MovieInformationLookups.GoogleMovieLookup(searchEngineMock.Object, imdbMock.Object);
                MovieInformation movieInformation;

                searchEngineMock.Setup(x => x.SearchWeb(string.Empty)).Returns(resultMock.Object);
                resultMock.Setup(x => x.Link).Returns("NOTHING USEFUL");

                // Act
                var isValid = lookup.TryParseResponse(string.Empty, out movieInformation);

                // Assert
                Assert.IsFalse(isValid);
            }

            [TestMethod]
            public void WhenTheFirstSearchResultContainAnIMDBLinkThenValid()
            {
                // Arrange
                var imdbMock = new Mock<IIMDBLookup>();
                var searchEngineMock = new Mock<IGoogleSearch>();
                var searchResult = new Result {Link = "http://www.imdb.com/title/tt0095016/"};

                var lookup = new BigMan.Services.MovieInformationLookups.GoogleMovieLookup(searchEngineMock.Object, imdbMock.Object);
                MovieInformation movieInformation;

                searchEngineMock.Setup(x => x.SearchWeb(null)).Returns(searchResult);

                // Act
                var isValid = lookup.TryParseResponse(null, out movieInformation);

                // Assert
                Assert.IsTrue(isValid);
            }
        }
    }
}