/* jQuery Plugins */
// http://stackoverflow.com/questions/6524288/jquery-event-for-user-pressing-enter-in-a-textbox
$.fn.pressEnter = function (fn) {
    return this.each(function () {
        $(this).bind('enterPress', fn);
        $(this).keyup(function (e) {
            if (e.keyCode == 13) {
                $(this).trigger("enterPress");
            }
        });
    });
};

$(document).ready(function () {
    // SignalR
    SignalRHelper.WireUpEvents();

    // Cached jQuery selectors
    App.WireUpEvents();

    // Wire up searching events
    SearchHelper.WireUpEvents();

    // Sidebar Toggle	
    SidebarHelper.WireUpEvents();

    // Waypoint
    WaypointHelper.WireUpEvents();

    // Modernizr SVG backup
    SVGHelper.WireUpEvents();

    // Background stretch	
    BackstretchHelper.WireUpEvents();

    // Typeahead
    TypeaheadHelper.WireUpEvents();

    // Wire up the recent searches
    ModelHelper.WireUpEvents();
    RecentlySearchedHelper.WireUpEvents();
    YearOnYearHelper.WireUpEvents();
    BestAndWorstHelper.WireUpEvents();
    AllTimeHelper.WireUpEvents();

    // Chart Helper
    RadialGaugeHelper.WireUpEvents();
});

window.App = {
    MoviePosterSearching: {},
    MovieNameSearchButtonIcon: {},
    MoviePoster: {},
    MovieNameSearch: {},
    NavExpander: {},
    Section1: {},
    Section2: {},
    Section3: {},
    Section4: {},
    Section5: {},
    Section6: {},
    NavSection1: {},
    NavSection2: {},
    DataSection: {},
    NavDataSection: {},
    WorstResultsShown: {},

    WireUpEvents: function () {
        var self = this;

        self.MoviePosterSearching = $('#MoviePosterSearching');
        self.MovieNameSearchButtonIcon = $('#MovieNameSearchButtonIcon');
        self.MoviePoster = $('#MoviePoster');
        self.NavExpander = $('#NavExpander');
        self.Section1 = $("#section1");
        self.Section2 = $("#section2");
        self.Section3 = $("#section3");
        self.Section4 = $("#section4");
        self.Section5 = $("#section5");
        self.Section6 = $("#section6");
        self.MovieNameSearch = $('#MovieNameSearch');
        self.NavSection1 = $('#nav li[data-section="1"]');
        self.NavSection2 = $('#nav li[data-section="2"]');
        self.WorstResultsShown = $('#WorstResultsWrapper').is(':visible');
        
        for (var i = 1; i < 7; i++) {
            self.DataSection[i] = $('.section[data-section="' + i + '"]');
            self.NavDataSection[i] = $('.navigation li[data-section="' + i + '"]');
        }
    }
};