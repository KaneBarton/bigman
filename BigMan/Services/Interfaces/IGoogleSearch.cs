﻿using Google.Apis.Customsearch.v1.Data;

namespace BigMan.Services.Interfaces
{
    public interface IGoogleSearch
    {
        Result SearchWeb(string movieName);
    }
}