﻿using System.Web.Mvc;

namespace BigMan.Portal.Controllers
{
    public class HomeController : Controller 
    {
        [System.Web.Mvc.AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }
    }
}