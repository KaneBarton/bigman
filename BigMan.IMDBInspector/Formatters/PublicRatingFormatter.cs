﻿using BigMan.Domain;
using BigMan.IMDBInspector.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;

namespace BigMan.IMDBInspector.Formatters
{
    public class PublicRatingFormatter : IDataFormatter
    {
        public static int Default = 7;

        public Movie CurrentEnumerator { get; private set; }

        public IEnumerable<Movie> Parse()
        {
            var lineNumber = 0;

            foreach (var line in File.ReadLines(Path.Combine(Program.BasePath, "ratings.list")))
            {
                lineNumber++;
                if (lineNumber % 50000 == 1) Console.Write(".");
                if (lineNumber < 299) continue;
                if (string.IsNullOrEmpty(line)) continue;
                if (line.StartsWith("------------------------")) break;

                var movieName = line.Substring(32).Trim();
                decimal rating;

                if (!decimal.TryParse(line.Substring(27, 3), out rating)) continue;

                CurrentEnumerator = new Movie(movieName) { PublicRatingMultipler = Convert.ToInt32(Math.Ceiling(rating)) };
                yield return CurrentEnumerator;
            }
        }

        public Movie ApplyValues(Movie source)
        {
            source.PublicRatingMultipler = CurrentEnumerator.PublicRatingMultipler;
            return source;
        }
    }
}