﻿using System.Collections.Generic;

namespace BigMan.Portal.Models
{
    public class BestAndWorstViewModel
    {
        public IEnumerable<MovieBasicViewModel> Best { get; set; }
        public IEnumerable<MovieBasicViewModel> Worst { get; set; } 
        public int Year { get; set; }
    }
}