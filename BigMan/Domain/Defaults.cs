﻿using BigMan.Annotations;

namespace BigMan.Domain
{
    public static class Defaults
    {
        public const int MinYear = 1970;
        public const int MaxManScore = 100;
        public const string Image = "/Portal/Content/Images/no-poster.jpg";
        public const string MovieName = "You gotta try searching for it...";
        private const string ManTag = "Life is a mystery to us all.";
        private const string Plot = "Life is a mystery to us all.";

        [NotNull]
        private static readonly MovieInformation StaticMovieInformation = new MovieInformation
        {
            MovieName = Defaults.MovieName.ToUpperInvariant(),
            Plot = Defaults.Plot,
            ManTag = Defaults.ManTag,
            ImageUrl = Defaults.Image
        };

        [NotNull]
        private static readonly Movie StaticMovie = new Movie
            {
                Name = Defaults.MovieName,
                NameUpper = Defaults.MovieName.ToUpperInvariant()
            };

        public static Movie DefaultMovie()
        {
            return StaticMovie;
        }

        public static MovieInformation DefaultMovieInformation()
        {
            return StaticMovieInformation;
        }
    }
}