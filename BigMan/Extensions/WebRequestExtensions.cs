﻿using System;
using System.IO;
using System.Net;
using BigMan.Domain;

namespace BigMan.Extensions
{
    public static class WebRequestExtensions
    {
        public static string HttpGet(string uri)
        {
            if (string.IsNullOrEmpty(uri)) return string.Empty;

            var req = System.Net.WebRequest.Create(uri);
            req.Timeout = 5 * 1000;
            req.Proxy = null;
            using (var resp = req.GetResponse())
            {
                var sr = new System.IO.StreamReader(resp.GetResponseStream());
                return sr.ReadToEnd().Trim();
            }
        }

        public static bool TryParseImdbId(string link, out string imdbId)
        {
            imdbId = string.Empty;

            if (!link.Contains("/tt")) return false;

            imdbId = link.Substring(link.IndexOf("/tt", System.StringComparison.InvariantCultureIgnoreCase) + 1);
            if (imdbId.EndsWith("/"))
            {
                imdbId = imdbId.Substring(0, imdbId.Length - 1);
            }

            return true;
        }
    }
}