﻿using BigMan.Annotations;
using BigMan.Domain;
using System.Collections.Generic;
using System.Linq;

namespace BigMan.Repository
{
    [UsedImplicitly]
    public class ManTagRepository : IRepository<ManTag>
    {
        private static IList<ManTag> _manTagList;

        IEnumerable<ManTag> IRepository<ManTag>.Retrieve()
        {
            return _manTagList ?? (_manTagList =
                                        System.IO.File.ReadLines(string.Format("{0}ManTag.txt", Configuration.BasePath))
                                                      .Select(line => line.Split('\t'))
                                                      .Select(items => new ManTag { Score = int.Parse(items[0]), Tag = items[1] }).ToList());
        }
    }
}