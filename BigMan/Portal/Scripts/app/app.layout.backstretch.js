﻿var BackstretchHelper = {
    WireUpEvents: function () {
        App.Section1.backstretch([
                "/Portal/Content/Images/man-movie-0.jpg",
                "/Portal/Content/Images/man-movie-1.jpg",
                "/Portal/Content/Images/man-movie-2.jpg",
                "/Portal/Content/Images/man-movie-3.jpg",
                "/Portal/Content/Images/man-movie-4.jpg",
                "/Portal/Content/Images/man-movie-5.jpg",
                "/Portal/Content/Images/man-movie-6.jpg",
                "/Portal/Content/Images/man-movie-7.jpg",
                "/Portal/Content/Images/man-movie-8.jpg",
                "/Portal/Content/Images/man-movie-9.jpg",
                "/Portal/Content/Images/man-movie-10.jpg",
                "/Portal/Content/Images/man-movie-11.jpg",
                "/Portal/Content/Images/man-movie-12.jpg",
                "/Portal/Content/Images/man-movie-13.jpg",
                "/Portal/Content/Images/man-movie-14.jpg"
        ], {
            fade: 750,
            duration: 6000
        });
    }
};