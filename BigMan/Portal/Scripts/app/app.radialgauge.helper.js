﻿var RadialGaugeHelper = {
    WireUpEvents: function () {
        $('.percentage').easyPieChart({
            animate: 750,
            barColor: function (percent) {
                if (percent > 80) {
                    return "#c0392b";
                }
                if (percent > 60) {
                    return "#d35400";
                }
                if (percent > 45) {
                    return "#f39c12";
                }
                if (percent > 33) {
                    return "#e67e22";
                }
                if (percent > 15) {
                    return "#2980b9";
                }
                return "#9b59b6";
            },
            onStep: function (value) {
                this.$el.find('span').text(~~value);
            }
        });
    },

    Update: function (score) {
        $('.percentage', '#ManScore').data('easyPieChart').update(score);
    },
}