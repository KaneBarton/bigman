﻿using System.Collections.Generic;

namespace BigMan.Services.Interfaces
{
    public interface IMovieNameSearchService
    {
        IEnumerable<string> Retrieve(string nameSearch);
    }
}