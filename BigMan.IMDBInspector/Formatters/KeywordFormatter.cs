﻿using BigMan.Domain;
using BigMan.IMDBInspector.Interfaces;
using BigMan.IMDBInspector.Models;
using System;
using System.Collections.Generic;
using System.IO;

namespace BigMan.IMDBInspector.Formatters
{
    public class KeywordFormatter : IDataFormatter
    {
        public Movie CurrentEnumerator { get; private set; }

        public IEnumerable<Movie> Parse()
        {
            var lineNumber = 0;
            var model = new KeywordModel();
            var modelDictionary = new Dictionary<string, KeywordModel>(); 

            foreach (var line in File.ReadLines(Path.Combine(Program.BasePath, "keywords.list")))
            {
                lineNumber++;
                if (lineNumber % 50000 == 1) Console.Write(".");
                if (lineNumber < 58315) continue;

                var movieName = line.Substring(0, line.IndexOf("\t", System.StringComparison.Ordinal)).Trim();
                var keyword = line.Substring(movieName.Length + 1).Trim();

                if (!modelDictionary.ContainsKey(movieName))
                {
                    model = new KeywordModel {Name = movieName};
                    modelDictionary.Add(model.Name, model);
                }

                modelDictionary[movieName].Keywords.Add(keyword);
            }

            foreach (var keywordModel in modelDictionary)
            {
                CurrentEnumerator = new Movie(keywordModel.Key) {KeywordsMultipler = keywordModel.Value.Score()};
                yield return CurrentEnumerator;
            }
        }

        public Movie ApplyValues(Movie source)
        {
            source.KeywordsMultipler = CurrentEnumerator.KeywordsMultipler;
            return source;
        }
    }
}