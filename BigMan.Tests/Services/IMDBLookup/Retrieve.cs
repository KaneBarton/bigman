﻿using BigMan.Annotations;
using BigMan.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BigMan.Tests.Services.IMDBLookup
{
    [UsedImplicitly]
    public partial class IMDBLookupTests
    {
        [TestClass]
        public class Retrieve
        {
            [TestMethod]
            public void WhenNotValidImdbNumberThenDefaultMovieInformation()
            {
                // Arrange
                var lookup = new BigMan.Services.MovieInformationLookups.IMDBLookup();

                // Act
                var movieInformation = lookup.Retrieve(string.Empty);

                // Assert
                Assert.AreSame(Defaults.DefaultMovieInformation(), movieInformation);
                Assert.AreEqual(Defaults.Image, movieInformation.ImageUrl);
            }

            [TestMethod]
            public void WhenValidImdbNumberThenPlotAndImageUrlCanBeMapped()
            {
                // Arrange
                var lookup = new BigMan.Services.MovieInformationLookups.IMDBLookup();

                // Act
                var movieInformation = lookup.Retrieve("tt0095016");

                // Assert
                Assert.IsNotNull(movieInformation);
                Assert.IsFalse(string.IsNullOrWhiteSpace(movieInformation.Plot));
                Assert.IsFalse(string.IsNullOrWhiteSpace(movieInformation.ImageUrl));
            }
        }
    }
}
