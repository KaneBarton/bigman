﻿using BigMan.Annotations;
using BigMan.Domain;
using BigMan.Portal.Models;
using BigMan.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using BigMan.Services.Interfaces;

namespace BigMan.Services
{
    [UsedImplicitly]
    public class YearByYearService : IYearByYearService
    {
        private readonly IRepository<Movie> _movieRepository;
        private static readonly IList<MovieBasicViewModel> YearByYearList = new List<MovieBasicViewModel>();

        public YearByYearService(IRepository<Movie> movieRepository)
        {
            _movieRepository = movieRepository;
        }

        public IEnumerable<MovieBasicViewModel> Retrieve()
        {
            if (YearByYearList.Count != 0) return YearByYearList;
            
            for (var i = DateTime.Now.Year; i > Defaults.MinYear; i--)
            {
                var year = i;

                var movies = _movieRepository.Retrieve().Where(x => x.Year.Equals(year)).ToList();
                var maxScore = movies.Max(x => x.Score());
                var movie = movies.FirstOrDefault(x => x.Score() == maxScore) ?? Defaults.DefaultMovie();

                YearByYearList.Add(new MovieBasicViewModel
                {
                    Year = year,
                    Score = maxScore,
                    Name = movie.Name,
                });
            }

            return YearByYearList;
        }
    }
}