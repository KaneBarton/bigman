﻿var YearByYearViewModelHelper = {
    WireUpEvents: function () {
        window.yearByYearViewModel = {
            SearchesLoading: ko.observable(true),
            SearchesLoaded: ko.observable(false),
            Results: ko.observableArray()
        };
        
        ko.applyBindings(window.yearByYearViewModel, App.Section5[0]);
    }
};