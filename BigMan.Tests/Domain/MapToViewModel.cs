﻿using System;
using BigMan.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BigMan.Tests.Domain
{
    public partial class MovieTests
    {
        [TestClass]
        public class MapToViewModel
        {
            [TestMethod]
            public void ModelCanBeMappedToViewModel()
            {
                // Arrange
                var movie = new Movie(Guid.NewGuid().ToString("N")) { Year = DateTime.Now.Millisecond };

                // Act
                var expected = movie.MapToViewModel();

                // Assert
                Assert.AreEqual(movie.Name, expected.Name);
                Assert.AreEqual(movie.NameUpper, expected.NameUpper);
                Assert.AreEqual(movie.Year, expected.Year);
                Assert.AreEqual(1, expected.Score);
            }
        }
    }
}
