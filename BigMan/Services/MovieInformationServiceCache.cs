﻿using System.Collections.Generic;
using BigMan.Domain;
using BigMan.Services.Interfaces;

namespace BigMan.Services
{
    public class MovieInformationServiceCache : IServiceCache<MovieInformation>
    {
        private static readonly IDictionary<string, MovieInformation> Cache = new Dictionary<string, MovieInformation>();
        private static readonly object MovieInformationLock = new object(); // singleton locks as we are using static variables, as there really isn't a need to use the HttpRuntime.Cache

        public void Add(MovieInformation domain)
        {
            if (Contains(domain)) return;

            lock (MovieInformationLock)
            {
                if (!Contains(domain))
                {
                    Cache.Add(domain.MovieName.ToUpperInvariant(), domain);
                }
            }
        }

        private bool Contains(MovieInformation domain)
        {
            return Cache.ContainsKey(domain.MovieName.ToUpperInvariant());
        }

        public bool ContainsByKey(string key)
        {
            return Cache.ContainsKey(key.ToUpperInvariant());
        }

        public MovieInformation Item(MovieInformation domain)
        {
            return Cache[domain.MovieName.ToUpperInvariant()];
        }

        public MovieInformation ItemByKey(string key)
        {
            return Cache[key.ToUpperInvariant()];
        }
    }
}